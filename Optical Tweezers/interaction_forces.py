# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 13:21:01 2024

@author: TAU
"""

#%% important
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
#%% wrap legends funciton
import textwrap
def wrap_labels(ax, width, break_long_words=False):
    labels = []
    for label in ax.get_xticklabels():
        text = label.get_text()
        labels.append(textwrap.fill(text, width=width,
                      break_long_words=break_long_words))
    ax.set_xticklabels(labels, rotation=0)
    

def signify(p_value):
    if p_value > 0.05:
        return 'ns'
    elif p_value <= 0.05 and p_value > 0.01:
        return '*'
    elif p_value <= 0.01 and p_value > 0.001:
        return '**'
    else:
        return '***'
#%% colormap for plotting
pal = sns.color_palette('Paired', 6)
#pal = flatten([[pal[i+1], pal[i]] for i in range(0,len(pal),2)])
pal = pal[::-1]
pal.pop(3), pal.pop(2)
pal = pal + [sns.color_palette('Paired', 12)[-1]]
#%% plot style

mpl.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["font.size"] = 12
plt.rcParams["axes.labelweight"] = "bold"
#plt.rcParams['savefig.transparent'] = True

#%% pullout forces
#read file
fpath = r"E:\alon for paper\figures\Figure 3\Final_Forces and fit.xlsx"
forcesdata = pd.read_excel(fpath, sheet_name=None)

for sheet in forcesdata:
    forcesdata[sheet]['sheetname'] = sheet

forcesdata = pd.concat([df for df in forcesdata.values()])
forces = forcesdata[['file_name', 'sheetname', 'BF']]
splitted =  forces['sheetname'].str.removeprefix('pH ').str.split(' ', expand=True)
forces[['ph', 'divalent', 'protein', 'speed']] = splitted

#drop the irrelevant condition
forces = forces[forces['speed'] != '10um_s']


#create youre labels
forces['condition'] = 'pH ' + forces['ph'] + ' + ' + forces['divalent']
forces.loc[forces['protein']=='StreptAvidin', 'condition'] = 'StreptA bead'
forces['label'] = forces['condition'].apply(lambda s: s.replace('+', '\n+'))
forces.loc[forces['condition']=='StreptA bead', 'label'] = 'StreptA \n bead'



#%% calculate average+SEM
vals = forces[['BF', 'condition']]
avs = vals.groupby('condition').mean()
ers = vals.groupby('condition').sem()
quant = vals.groupby('condition').quantile([0.25,0.75])
res = avs.round(2).astype(str) + ' +- ' + ers.round(2).astype(str)
res.to_csv(r"E:\alon for paper\figures\Figure 3\forces_mean+SEM.csv")
quant.to_csv(r"E:\alon for paper\figures\Figure 3\forces_quantiles.csv")
print(res)
print(quant)

#%% and calculate mann whitney p value for all
from scipy.stats import mannwhitneyu

vals = forces[['BF', 'condition']]
#conds = vals['condition'].unique().sort()
Ps = pd.DataFrame()
for condition1, gr1 in vals.groupby('condition'):
    for condition2, gr2 in vals.groupby('condition'):
        U1, p = mannwhitneyu(gr1['BF'], gr2['BF'], method = 'exact')
        Ps.loc[condition1, condition2] = round(p, 4)

print('\n', 'p values: \n', Ps)
Ps.to_csv(r"E:\alon for paper\figures\Figure 3\forces_p_values.csv")

#%% now plot
order = forces['condition'].unique()
order.sort()
order = np.roll(order, -1)
#order.append(order.pop(-1))

#bar plot
fig, ax= plt.subplots()
fig.set_size_inches(4,4)
sns.barplot(
    data=forces, x='condition', y='BF', palette=pal, #hue='label',
    estimator='mean',
    order=order, 
    #width = 0.7,
    edgecolor = "0", linewidth = 1.5,
    errorbar=('pi',50), capsize = 0.2, errwidth = 1.5, errcolor = ".4",
    ax =ax)

#datapoints
sns.stripplot(
data=forces, x='condition', y='BF',
    color = 'k', size = 3, alpha=0.7,
    #color='w', alpha =1, edgecolor='k', linewidth=1, size=3,
    #palette=pal, alpha =1, edgecolor='k', linewidth=1, size=3,
    #jitter=False,
    order = order,
    )

#ax.set(ylim=(0,250))
ref_group = 'pH 5.2 + Ca'
sig_signs = Ps.applymap(signify)
for cond, sig in sig_signs[ref_group].items():
    #print(cond, sig)
    if not cond==ref_group:
        if sig == 'ns':
            siz = 14
            y_factor = 1.05
        else:
            siz = 18
            y_factor = 1.025
        ax.text(x=cond, y=ax.get_ylim()[1]*y_factor, s=sig, ha='center', va='center', size = siz)
        ax.text(x=cond, y=ax.get_ylim()[1]*1.05, s='___', ha='center', va='center', size = 16, weight='normal')

wrap_labels(ax, 6)

sns.despine()
ax.set(ylabel = 'Force (pN)', 
       xlabel = '')
plt.yticks(size=12)
fig.set_dpi(200)
plt.savefig(r"E:\alon for paper\figures\Figure 3\interaction forces differrent conditions.pdf", bbox_inches = 'tight' )
#plt.show()
ax.set_xticklabels('')
plt.savefig(r"E:\alon for paper\figures\Figure 3\interaction forces differrent conditions - no labels.pdf",  bbox_inches = 'tight', transparent=True)
#plt.show()
#%%