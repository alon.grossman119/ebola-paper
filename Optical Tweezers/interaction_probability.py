# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 13:21:01 2024

@author: TAU
"""

#%% important
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
#%% wrap legends funciton
import textwrap
def wrap_labels(ax, width, break_long_words=False):
    labels = []
    for label in ax.get_xticklabels():
        text = label.get_text()
        labels.append(textwrap.fill(text, width=width,
                      break_long_words=break_long_words))
    ax.set_xticklabels(labels, rotation=0)
    
def signify(p_value):
    if p_value > 0.05:
        return 'ns'
    elif p_value <= 0.05 and p_value > 0.01:
        return '*'
    elif p_value <= 0.01 and p_value > 0.001:
        return '**'
    else:
        return '***'

#%% colormap for plotting
pal = sns.color_palette('Paired', 6)
#pal = flatten([[pal[i+1], pal[i]] for i in range(0,len(pal),2)])
pal = pal[::-1]
pal[2:] = pal[4:6] + pal[2:4]
pal.insert(4, 'darkgrey')

#%% plot style

mpl.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["font.size"] = 12
plt.rcParams["axes.labelweight"] = "bold"
#plt.rcParams['savefig.transparent'] = True


#%% probabilty of interactions
#read file
ppath = r"E:\alon for paper\figures\Figure 3\Probability of interactions 0.01 GPdTM.csv"
probs = pd.read_csv(ppath)
# create your labels
probs['ph'] = probs['ph'].astype(str)
probs['condition'] = 'pH ' + probs['ph'] + ' + ' + probs['divalent']
probs.loc[probs['protein']=='no','condition'] = 'no GPdTM'
probs['label'] = probs['condition'].apply(lambda s: s.replace('+', '\n+'))
probs.loc[probs['condition']=='no GPdTM', 'label'] = 'no \n GPdTM'


#%% calculate average+SEM
vals = probs[['probability', 'condition']]
avs = vals.groupby('condition').mean()
ers = vals.groupby('condition').sem()
res = avs.round(2).astype(str) + ' +- ' + ers.round(2).astype(str)
res.to_csv(r"E:\alon for paper\figures\Figure 3\probs_mean+sem.csv")
print(res)
#res.to_csv(r"E:\alon for paper\figures\Figure 3\prob_results.csv")

#%% and calculate mann whitney p value for all
from scipy.stats import mannwhitneyu

vals = probs[['probability', 'condition']]
#conds = vals['condition'].unique().sort()
Ps = pd.DataFrame()
for condition1, gr1 in vals.groupby('condition'):
    for condition2, gr2 in vals.groupby('condition'):
        U1, p = mannwhitneyu(gr1['probability'], gr2['probability'], method = 'exact')
        Ps.loc[condition1, condition2] = round(p, 4)

print('\n', 'p values: \n', Ps)
Ps.to_csv(r"E:\alon for paper\figures\Figure 3\probabilities_p_values.csv")
#%% now plot the probabilities
order = probs['condition'].unique()
order.sort()
order = np.roll(order, -1)
#order.append(order.pop(-1))

#barplot
fig, ax= plt.subplots()
fig.set_size_inches(4,4)
sns.barplot(
    data=probs, x='condition', y='probability', palette=pal, #hue='label',
    order=order,
    edgecolor = "0", linewidth = 1.5,
    errorbar='se', capsize = 0.2, errwidth = 1.5, errcolor = ".4",
    ax =ax)

#data points
sns.stripplot(
data=probs, x='condition', y='probability',
    #color = 'k', size = 2,
    color = 'k', size = 3, alpha=0.8,
    #color='w', alpha =1, edgecolor='k', linewidth=1, size=3,
    #palette=pal, alpha =1, edgecolor='k', linewidth=1, size=3,
    order = order,
    )


#significance
ref_group = 'pH 5.2 + Ca'
sig_signs = Ps.applymap(signify)
for cond, sig in sig_signs[ref_group].items():
    #print(cond, sig)
    if not cond==ref_group:
        if sig == 'ns':
            siz = 14
            y_factor = 1.05
        else:
            siz = 18
            y_factor = 1.025
        ax.text(x=cond, y=ax.get_ylim()[1]*y_factor, s=sig, ha='center', va='center', size = siz)
        ax.text(x=cond, y=ax.get_ylim()[1]*1.05, s='___', ha='center', va='center', size = 16, weight='normal')

#finish
wrap_labels(ax, 6)
sns.despine()
ax.set(ylabel = 'Probabilty of Interaction (%)', 
       xlabel = '')
plt.yticks(size=12)
fig.set_dpi(200)
plt.savefig(r"E:\alon for paper\figures\Figure 3\prob of interactions differrent conditions.pdf",  bbox_inches = 'tight')
ax.set_xticklabels('')
plt.savefig(r"E:\alon for paper\figures\Figure 3\prob of interactions differrent conditions - no labels.pdf",  bbox_inches = 'tight', transparent=True)