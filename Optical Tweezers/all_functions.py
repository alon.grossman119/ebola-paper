# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 16:40:15 2023

@author: Alon Grossman
"""
import lumicks.pylake as lum
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np
import matplotlib
import pickle
import glob
import pandas as pd
from scipy.signal import find_peaks, peak_prominences
from scipy.ndimage import gaussian_filter
from itertools import cycle
from cycler import cycler
#%%

#%% subtracting approach from detachment
def extract_and_downsample(file, frq, method = 'ceil', force_channel='Force 2x', dist_channel = 'Piezo Distance'):
    ds_force= file['Force HF'][force_channel].downsampled_to(frq, method=method)
    ds_distance = file['Distance'][dist_channel].downsampled_to(frq, method=method)
    
    ds_timestamp = ds_force.timestamps
    
    return ds_force.data, ds_distance.data, ds_timestamp

def extract_HF(file, force_channel='Force 2x', dist_channel = 'Piezo Distance'):
    HF_force = file['Force HF'][force_channel].data
    HF_distance = file['Distance'][dist_channel].data
    HF_timestamps = file['Force HF'][force_channel].timestamps
    
    return HF_force, HF_distance, HF_timestamps

def extract_and_downsample_by(file, fac, force_channel='Force 2x', dist_channel = 'Piezo Distance'):
    ds_force= file['Force HF'][force_channel].downsampled_by(fac)
    ds_distance = file['Distance'][dist_channel].downsampled_by(fac)

    ds_timestamp = ds_force.timestamps
    return ds_force.data, ds_distance.data, ds_timestamp

def substruct_approach_from_detachment(file, approach_file, frq, **ext_kwargs):
    force_detach, distance_detach, timestamp_detach = extract_and_downsample(file, frq, **ext_kwargs)
    force_approach, distance_approach, timestamp_approach = extract_and_downsample(approach_file, frq, **ext_kwargs)
    
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    timestamp_detach = timestamp_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    #distance_detach, force_detach = (np.array(t) for t in zip(*sorted(zip(distance_detach, force_detach))))
    
    return distance_detach, force_detach, timestamp_detach

def substruct_approach_from_detachment_HF(file, approach_file, **ext_kwargs):
    force_detach, distance_detach, timestamp_detach = extract_HF(file, **ext_kwargs)
    force_approach, distance_approach, timestamp_approach = extract_HF(approach_file, **ext_kwargs)
    
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    timestamp_detach = timestamp_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    
    return distance_detach, force_detach, timestamp_detach



def substruct_approach_from_detachment_fac(file, approach_file, fac, **ext_kwargs):
    force_detach, distance_detach, timestamp_detach = extract_and_downsample_by(file, fac, **ext_kwargs)
    force_approach, distance_approach, timestamp_approach = extract_and_downsample_by(approach_file, fac, **ext_kwargs)
    
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    timestamp_detach = timestamp_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    
    return distance_detach, force_detach, timestamp_detach



def substruct_approach_from_detachment_old(file, approach_file):
    
    ds_detach = file.downsampled_force2x
    distance_detach, force_detach = file['Distance']['Piezo Distance'].downsampled_like(ds_detach)
    timestamp_detach = force_detach.timestamps
    distance_detach, force_detach = distance_detach.data, force_detach.data
    
    ds_approach = approach_file.downsampled_force2x
    distance_approach, force_approach = approach_file['Distance']['Piezo Distance'].downsampled_like(ds_approach)
    distance_approach, force_approach = distance_approach.data, force_approach.data
    
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    timestamp_detach = timestamp_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    distance_detach, force_detach = (np.array(t) for t in zip(*sorted(zip(distance_detach, force_detach))))
    
    return distance_detach, force_detach, timestamp_detach

def substruct_approach_from_detachment_inbal(file, approach_file, frq=None):
    ds_detach = file['Distance']['Distance 1']
    force_detach, distance_detach = file['Force HF']['Force 2x'].downsampled_like(ds_detach)
    timestamp_detach = force_detach.timestamps
    distance_detach, force_detach = distance_detach.data, force_detach.data
    
    ds_approach = approach_file['Distance']['Distance 1']
    force_approach, distance_approach = approach_file['Force HF']['Force 2x'].downsampled_like(ds_approach)
    distance_approach, force_approach = distance_approach.data, force_approach.data
    
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    timestamp_detach = timestamp_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    #distance_detach, force_detach = (np.array(t) for t in zip(*sorted(zip(distance_detach, force_detach))))
    
    return distance_detach, force_detach, timestamp_detach


def substruct_approach_from_detachment_older(file, approach_file):
    force_detach = file.force2x.data
    distance_detach = file['Distance']['Piezo Distance'].data
    force_approach = approach_file.force2x.data
    distance_approach = approach_file['Distance']['Piezo Distance'].data
    distance_approach, force_approach = (np.array(t) for t in zip(*sorted(zip(distance_approach, force_approach))))
    force_detach = force_detach[min(distance_approach) <= distance_detach]
    distance_detach = distance_detach[min(distance_approach) <= distance_detach]
    force_detach = force_detach - np.interp(distance_detach, distance_approach, force_approach)
    return distance_detach, force_detach

#%% filename handling

def getnum(path):
    if type(path)==str:    
        path = Path(path)
    return path.stem.split(' ')[-1]

def getSerNum(path):
    if type(path)==str:    
        path = Path(path)
    return path.name.split(' ')[0]

def getFilebySerNum(dirs, serNum):
    paths = [item for item in dirs if getSerNum(item) in serNum]
    if len(paths)==0:
        print('no paths found!')
    return paths

def getFDsFromFolder(path):
    FDs = glob.glob(str(path+ '\\*FD curve*.h5'), recursive=True)
    return FDs

def getCurveNum(path):
    if type(path)==str:    
        path = Path(path) 
    s= path.name.split(getSerNum(path))
    if len(s) == 2:
        s = s[1]
    return s.strip().removesuffix('.h5')

def moveCol(df, colname, position):
    df.insert(position, colname, df.pop(colname))
#%% interaction 

def isInteraction(data, thresh=10):
    if data.max() < thresh:
        return False
    else:
        return True


#%% df classification

def filt_bottom(df, low_thresh=4):
    mask = df['force'] < low_thresh
    df.loc[mask, 'type'] = 'bottom'
    return df

def def_start_finish(df):
    #check_bottom_groups(df)

    firstgroup = df.loc[df['type'] == 'bottom', 'group'].min()
    lastgroup = df.loc[df['type'] == 'bottom', 'group'].max()

    # can also be written as df.loc[(df['group']==firstgroup)]['type'] == 'start'
    df.loc[(df['type'] == 'bottom') & (
        df['group'] == lastgroup), 'type'] = 'finish'
    df.loc[(df['type'] == 'bottom') & (
        df['group'] == firstgroup), 'type'] = 'start'
    
    
    return df

def def_start_finish_alt(df):
    gro = df['group'].unique()
    df.loc[(df['type'] == 'bottom') & (df['group']==gro[0]), 'type'] = 'start'
    df.loc[(df['type'] == 'bottom') & (df['group']==gro[-1]), 'type'] = 'finish'
    return df
    
def interaction(df):
    if np.all(df['type'].unique()=='start'):
        return False
    else:
        return True

def filt_descent(df): #this function is written poorly, I should rewrite it in a smarter way.
    if not has_finish(df):
        finishpoint = df.tail(1)
    else:
        finishpoint = df[df.type == 'finish'].head(1)
    points = df.loc[df.index.values < finishpoint.index.values]

    for i in range(points.iloc[[-1]].index[0], 0, -1):
        row = df.loc[[i]]
        prevrow = df.loc[[i-1]]
        diforce = row['force'].values-prevrow['force'].values
        didist = row['distance'].values-prevrow['distance'].values
        deriv = (diforce)/(didist)
        
        if diforce>0:
            if didist<0:
                res= df.loc[[i+1]].index[0]
                break
        
        if deriv>0:
            res= df.loc[[i+1]].index[0]
            break
    
    df.loc[(res):(points.tail(1).index[0]), 'type'] = 'descent'
    return df

def filt_overstretch(df, bot=63.5, top=70):
    mask = (df['force'] > bot) & (df['force'] < top)
    df.loc[mask, 'type'] = 'overstretch'
    return df

def split_ascend(df): #if you've one all of the other filterings correctly and in the right manner, you should have had everything figured excpet part of increasing force.
    df.loc[df['type'].isnull(), 'type'] = 'ascend'
    remains = df.loc[df['type']== 'ascend'].copy()
    remains = parse_groups(remains)
    gr = dict(remains.groupby('group').groups)
    #assign the first group as ascend1
    idxs = gr[min(gr)]
    df.loc[idxs, 'type'] = 'ascend1'
    #assign the last group as ascend2- if it exsits
    if min(gr) != max(gr):
        idxs = gr[max(gr)]
        df.loc[idxs,'type'] = 'ascend2'
    return df


def find_turns(df, **props):
    maxs, _ = find_peaks(df.force, **props)
    mins, _ = find_peaks(-df.force, **props)
    
    maxs = df.iloc[maxs].index.values
    mins = df.iloc[mins].index.values
    
    return maxs, mins

def split_ascend1(df, **props):
    cut = df.loc[df['type']=='ascend1'].copy()
    maxs, mins = find_turns(cut, **props)
    
    st = cut.head(1).index.values-1
    en = cut.tail(1).index.values
    #lims = (df.iloc[[-1+num]], cut.iloc[maxs], cut.iloc[mins], cut.iloc[[-1]])
    #lims = np.concatenate([v.index.values for v in lims])
    lims = np.concatenate([st, maxs, mins, en])
    lims.sort()
    
    m = df['group'].max()+1
    for i in range(len(lims)-1):
        cut.loc[(cut.index>lims[i]) & (cut.index<=lims[i+1]), 'group'] = i + m

    df.loc[cut.index, 'group'] = cut['group']
    
    return df

def fill_interaction(df):
    df.loc[df['type'].isnull(), 'type'] = 'interaction'
#%% lumicks fit utilis
def get_params_from_fit(fit):
    #this function recieves a pylake.FDfit object and returns all the fit parameters as a dictionary
    param_list = list(fit.params)
    d = {key: fit[key].value for key in param_list}
    #l=[fit[item].value for item in param_list]
    return d

def getCurveFromModel(fit, independant_var):
    #this function recieves a pylake.FDFit object and a numpy array of the independat variable of that fit.
    #It returns a numpy array of the other variable, according the the fitted parameters in the model
    model = list(fit.models.values())[0]
    dependent_var =  model(independant_var, get_params_from_fit(fit))
    return dependent_var

#%% df utils

def parse_groups(df):
    df['group'] = -1
    tps = df['type'].unique()
    tps = tps[tps != None]

    for tp in tps:
        # calculate  difference in the index to determin if there are time jumps
        j = np.diff(df.loc[df['type'] == tp].index) > 1
        # add first one manualy- zero difference beacause there is no jump.
        j = np.append(0, j)
        g = j.cumsum()  # use cumsum to determine groups
        df.loc[df['type'] == tp, 'group'] = g + df['group'].max()+1

    return df

def renumber_groups(df):
    c= df.copy()
    for newnum, oldnum in enumerate(df['group'].unique()):
        df.loc[c['group']==oldnum, 'group'] = newnum
    return df

def check_bottom_groups(df):
    return df.loc[df['type'] == 'bottom', 'group'].nunique() != 2
    #if df.loc[df['type'] == 'bottom', 'group'].nunique() != 2:
     #   raise('wrong number of bottom groups found')

def has_finish(df):
    return ('finish' in df['type'].unique())

def type_draw(df, **kwargs):
    types = ['start', 'finish', 'overstretch', 'descent', 'ascend2']

    fig, ax = plt.subplots()
    for i, key in enumerate(types):
        rel = df.loc[df.type==key]
        if len(rel)>0:
            ax.plot(rel['distance'], rel['force'], color = f'C{i}', label=key, **kwargs)

    rel = df.loc[df.type=='ascend1']
    
    custom_cycler = (cycler(color=['c', 'm', 'y', 'k']))
    ax.set_prop_cycle(custom_cycler)
    for g, d in rel.groupby('group'):
        ax.plot(d.distance, d.force, label = 'ascend1', **kwargs)
    ax.legend()
    
    return fig, ax

def weak_curve(df, thresh = 10):
    return df['force'].max()<thresh
    '''
    if df['force'].max()<thresh:
        return False
    else:
        return True
    '''

def get_BF(df):
    descent_point = df.loc[df['type']=='descent'].index.min()
    finish_point = df.loc[df['type']=='finish'].index.min()
    last_point = df.tail(1).index.values[0]
    if not np.isnan(descent_point):
        return df.loc[descent_point-1, 'force']
    elif not np.isnan(finish_point):
        return df.loc[finish_point-1, 'force']
    else:
        return df.loc[last_point, 'force']

def get_BF_alt(df):
    return df.loc[df['type']=='interaction'].iloc[-1]['force']

def extract_curve(savedir, df, hf=None, fit=None, info=None):
    #this function will save the given dataframe df in savedir as an xlsx file. given an pylake.FDFit object ('fit'),
    #it will save it too in the same file. Given a dataframe 'info' it will save it too.
    
    if fit is not None:
        distance =  np.arange(0.1,2, 0.01)
        force = getCurveFromModel(fit, distance)
        modelCurve = pd.DataFrame(dict(distance = distance, force= force))
    with pd.ExcelWriter(savedir+'.xlsx') as writer:
        df.to_excel(writer, sheet_name='FD curve', index=True)
        if fit is not None:
            modelCurve.to_excel(writer, sheet_name='fitted_model', index=True)
        if info is not None:
            info.to_excel(writer, sheet_name='info', index=True)
        
            


#%% data utils
def monotoneous(array):
    return (np.all(np.diff(array)>=0))

def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

def back_stepping(arr, smo = 5, thresh=0, edge=0.05):
    mi, ma = arr.min()+edge, arr.max()-edge
    arr = arr[(arr>mi) & (arr<ma)]
    di = np.diff(arr)
    di = smooth(di, smo)
    di = di[(smo):-(smo)]
    return np.any(di<thresh)

def back_stepping_old(arr, smo = 5, thresh=0):
    di = np.diff(arr)
    di = smooth(di, smo)
    return np.any(di<thresh)
#%% program utils
def save_variables_to_file(filename, *variables):
    """
    Save Python variables to a file using pickle.

    :param filename: The name of the file to save the variables to.
    :param variables: One or more Python variables to save.
    """
    try:
        with open(filename, 'wb') as file:
            for var in variables:
                pickle.dump(var, file)
        print(f'Successfully saved variables to {filename}')
    except Exception as e:
        print(f'Error saving variables: {e}')
              
def load_variables_from_file(filename):
    """
    Load Python variables from a file using pickle.

    :param filename: The name of the file to load variables from.
    :return: A tuple of loaded variables.
    """
    loaded_variables = ()
    try:
        with open(filename, 'rb') as file:
            while True:
                try:
                    variable = pickle.load(file)
                    loaded_variables += (variable,)
                except EOFError:
                    break
        return loaded_variables
    except Exception as e:
        print(f'Error loading variables: {e}')


def flatten(l):
    return [item for sublist in l for item in sublist]


#%% overstretch determination

def refine_overstretch(df, est, itv=10, noise=0.3):
    rel = df.loc[df['type'].isnull()] #take only the undefined values
    ost = rel.loc[(rel['force']>(est-itv)) & (rel['force']<(est+itv))] #take a broad estimation for an overstretch
    b= list(np.diff((np.diff(ost['force'])>noise))) #get a list of whether the plot changes direction 
    i1, i2 = b.index(True)+1, b[::-1].index(True)+1 #indexes of where the plot changes direction
    
    temp = ost.iloc[i1:-i2] #cut places where plot is fine
    low, high = temp['force'].min(), temp['force'].max()
    
    ost_idxs = (rel.loc[(rel['force']>=low) & (rel['force']<=high)]).index
    df.loc[ost_idxs, 'type'] = 'overstretch'
    
    return df

def refine_overstretch_alt(df, est, itv=10, noise=0.3):
    rel = df.loc[df['type'].isnull()] #take only the undefined values
    ost = rel.loc[(rel['force']>(est-itv)) & (rel['force']<(est+itv))] #take a broad estimation for an overstretch
    b= list(np.diff((np.diff(ost['force'])>noise))) #get a list of whether the plot changes direction 
    i1, i2 = b.index(True)+1, b[::-1].index(True)+1 #indexes of where the plot changes direction
    
    ost_idxs = ost.iloc[i1:-i2].index
    df.loc[ost_idxs, 'type'] = 'overstretch'
    
    return df

def estimate_overstretch(df, sigma=7, thresh = 0.25):
    #cut everything thats already defined
    rel = df.loc[df['type'].isnull()].copy()
    #rel = df.loc[((df['type']!='finish') & (df['type'] != 'descent') & (df['type'] != 'start'))].copy()
    #convolution with a gaussian derivative to (rougly) devide the data into places in which the derivative is 0
    deriv = (gaussian_filter(rel['force'], sigma, order = 1, mode='nearest'))
    # find where dreivative is ~0
    mask = deriv<thresh
    #devide to groups by the mask
    rel['type'] = mask
    parse_groups(rel)
    #calc group length
    lengths = rel[mask].groupby('group').count()
    
    if lengths.empty:
        return None
    #get the largest group
    gr = lengths.idxmax()[0]
    if lengths.loc[gr, 'force']<3:
        return None
    est = rel.loc[rel['group']==gr, 'force'].mean()
    if est<40:
        return None
    return est


#%%

def read_forcefiles(FD_path, frq=None, fac=None):
    num = getnum(FD_path)
    forceInt = lum.File(FD_path)
    markerInt = lum.File(glob.glob(str(FD_path.parent / f'*Marker {num}.h5'))[0])
    if frq:
        return substruct_approach_from_detachment(forceInt, markerInt, frq)
    if fac:
        return substruct_approach_from_detachment_fac(forceInt, markerInt, fac)
    else:
        return substruct_approach_from_detachment_HF(forceInt, markerInt)



def remove_HFline(ax):
    hf_line = [line for line in ax.get_lines() if line.get_label()=='HF_data']
    if hf_line:
        hf_line[0].remove()

def HFline_exists(ax):
    return True if [line for line in ax.get_lines() if line.get_label()=='HF_data'] else False

def get_HFline(ax):
    return [line for line in ax.get_lines() if line.get_label()=='HF_data']

def replace_df_data(d, values):
    d.drop(d.index, inplace=True) #clean the dataframe
    
def get_model_lines(ax):
    lines = ax.get_lines()
    models = [line for line in lines if ('model' in line.get_label())]
    return models

def remove_model_lines(ax):
    lines = get_model_lines(ax)
    for line in lines:
        line.remove()

def get_empty_axes(fig):
    axes = fig.get_axes()
    axes = [ax for ax in axes if ax.get_title()==""]
    return axes

def del_button_axes(fig):
    for ax_obj in get_empty_axes(fig):
        fig.delaxes(ax_obj)

#%% another function
