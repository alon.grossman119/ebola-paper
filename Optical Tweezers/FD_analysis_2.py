# -*- coding: utf-8 -*-

"""
Created on Mon Oct 30 14:07:47 2023

@author: Alon Grossman
"""

import lumicks.pylake as lum
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np
import matplotlib
import pickle
import glob
import pandas as pd
import os
from scipy.signal import find_peaks, peak_prominences
from scipy import stats
#from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from scipy.ndimage import gaussian_filter
from all_functions_2 import *

from matplotlib.widgets import Button
from tqdm import tqdm
#matplotlib.rcParams['figure.max_open_warning'] = 0
#%%



#%%

folders = [r"E:\alon for paper\data - tweezers\20240220",
           r"E:\alon for paper\data - tweezers\20240221",
           r"E:\alon for paper\data - tweezers\20240222",
           r"E:\alon for paper\data - tweezers\20240223",
           ]
sessdirs = flatten([glob.glob(d+'\\*session*\\') for d in folders])
sessdirs = [item.removesuffix('\\') for item in sessdirs if not 'results' in item]

sessdirs = [r"E:\alon for paper\data - tweezers\20240223\1st session- ABS EDTA"]



def_frq=10 
factor = 10
alt_factor = 5

max_force_for_fit = 30

FIT=True

save_all = True
save_interaction = True
#plot_all=True
#plot_interaction =True
#%%
frq = 10
max_force = 30
lowlim = 20

#%%
def set_ax_settings(ax):
    tol = 8
    ost = 60
    xlims = (0, 3.5)
    
    ax.patch.set_alpha(0.3)
    ax.set_xlim(xlims)
    ax.set_ylim(-4, 200)
    ax.axhline(y=ost, color='k', linestyle='--', alpha=0.5) 
    ax.axhline(y=lowlim, color='k', linestyle='--', alpha=0.5) 
    
    #ax.fill_between(np.arange(xlims[0], xlims[1]+1), ost-8, ost+8, alpha=0.2, color='k')

def read_HF(FD_path, fac=factor):
    distance, force, time = read_forcefiles(FD_path, fac=fac)
    return pd.DataFrame(dict(time = time, distance = distance, force = force, group=-1, type=None))
    
def draw_fit(fit, cmm, ax):
    nu = list(fit.data.keys())[0]
    fit.plot(str(nu), cmm, np.arange(0.1,2, 0.01), plot_data=True, markersize=5*2)
    mod = ax.get_lines()[-2]
    dat = ax.get_lines()[-1]
    mod.set_zorder(10)
    mod.set_label('model')
    dat.set_zorder(0)
    dat.set_label('model_data')
    

def set_model(name, force, distance):
    model = lum.ewlc_odijk_force("DNA") + lum.force_offset("DNA")
    fit = lum.FdFit(model)
    fit.add_data(name, force, distance)
    
    fit["DNA/Lp"].value =40
    fit["DNA/Lp"].lower_bound = 2
    fit["DNA/Lp"].upper_bound = 300
    fit["DNA/Lc"].value = 1
    fit["DNA/St"].value = 470
    return fit


def treat(df):
    filt_bottom(df)
    parse_groups(df)
    def_start_finish_alt(df)
    parse_groups(df)
    if interaction(df): 
        filt_descent(df)
        fill_interaction(df)
        parse_groups(df)
        
    return df

def deviation(data, model):
    #this functions recieves two np arrays of the same size, one represting the measured data and the other the the model (expected) value at the same point.
    #it computes persons coefficient, mean abs error and mean sqaure error and returns them as a dict
    r2 = stats.pearsonr(data, model)[0]**2
    mae = np.mean(np.abs(data - model))
    mse = np.mean((data - model)**2)
    return dict(R2=r2, MAE = mae, MSE = mse)


def check_single(df, res):
    if res['OST']:
        return True
    
    lc = res['DNA/Lc']
    lp = res['DNA/Lp']
    
    if res['BF']<75:  
        if lc<=1.2 and lc>=0.8:
            return True
        if lp<=75 and lp>=20:
            return True
        
    return False
    
def final_adjustments(results):
    #finish the results df
    #sort index
    results.index = results.index.astype(int)
    results = results.sort_index()
    
    #add columns for sernum and FDnum
    results['ID'] = results['file_name'].apply(getSerNum)
    results['CurveNum'] = results['file_name'].apply(getCurveNum)
    moveCol(results, 'ID', 1)
    moveCol(results, 'CurveNum', 2)

    #rename columns that start with 'DNA/
    results.columns = [item.removeprefix('DNA/') for item in list(results.columns)]
    
    #change 'interaction' parameter into human language
    results['interaction'] = results['interaction'].apply(lambda var: 'YES' if var==True else 'no')
    return results


def process_n_save(results, p):
    #summery scheme for probability of iteraction
    cs = results.groupby('interaction').count()['ID']
    ps = round(cs/cs.sum(), 2)
    scheme_interaction = pd.concat([ps, cs], axis=1)
    scheme_interaction.columns=['frac','num']
    
    #create a summery scheme for interaction types
    cs = results.groupby('type').count()['ID']
    ps = round(cs/cs.sum(), 2)
    scheme_types = pd.concat([ps, cs], axis=1)
    scheme_types.columns=['frac','num']
    
    #create a dataframe with only singles
    singles = results.loc[results['type']=='single']
    
    
    #results.to_csv(p)
    with pd.ExcelWriter(p.with_suffix('.xlsx')) as writer:
        results.to_excel(writer, sheet_name='results', index=True)
        scheme_interaction.to_excel(writer, sheet_name='prob. interaction', index=True)
        scheme_types.to_excel(writer, sheet_name='scheme', index=True)
        singles.to_excel(writer, sheet_name='singles', index=True)
        singles.to_excel(writer, sheet_name='singles_filtered', index=True)


#%%
fit_params = list(set_model(0, [0] ,[0]).params)
fit_quality_params = ['R2', 'MAE', 'MSE', 'problem']
basic_info  = ['file_name', 'interaction', 'type', 'BF', 'OST', 'timestep']
collected_results = (basic_info+fit_params+fit_quality_params) if FIT else basic_info


#%% same loop but segregated

fails = []
allFDs = []
for folder in sessdirs:
    print('analyzing ', folder)
    #get all FD curve files:
    FDs = getFDsFromFolder(folder)
    allFDs.append(FDs)
    
    #creat a folders to save the results
    resfold = Path(folder+'_results_auto')
    resfold_int = resfold / 'interaction'
    resfold_non = resfold / 'no int'
    try: os.mkdir(resfold)
    except: FileExistsError
    try: os.mkdir(resfold_int)
    except: FileExistsError
    try: os.mkdir(resfold_non)
    except: FileExistsError
    
    #create a dataframe to save the resilts
    results = pd.DataFrame(columns=collected_results)
        
    #sort the files into interacting and non
    int_mask=[]
    frq=def_frq
    for FD_path in tqdm(FDs, desc = f'probing {len(FDs)} interactions'):
        FD_path = Path(FD_path)
        try:
            _, force, _ = read_forcefiles(FD_path, frq)
            int_mask.append(isInteraction(force, thresh=lowlim))
        except:
            print('problem!')
            int_mask.append(True)
    ints = np.array(FDs)[int_mask]
    nonts = np.array(FDs)[np.logical_not(int_mask)]
    
    
    #if this folder has different speed, assign it to freq
    n = Path(folder).name
    if 'umsec' in n:
        sp = float(n.split('umsec')[0].split(" ")[-1])
        frq = sp*100
        fac= alt_factor
    else: 
        frq = def_frq
        fac = factor

    
    #now iterate over the interactions ones:
    #%matplotlib inline
    for FD_path in tqdm(ints, desc = f'processing {len(ints)} interactions'):
        FD_path = Path(FD_path)
        fit = None
        
        #generate result dictionary
        res = {key: None for key in collected_results}
        res['file_name'] = str(FD_path)
        res['interaction'] = True
        res['type'] = 'undef'
        
        #try to open and handle the file
        try:
            distance, force, time = read_forcefiles(FD_path, frq)
            df = pd.DataFrame(dict(time = time, distance = distance, force = force, group=-1, type=None))
            df = treat(df)
            hf = read_HF(FD_path, fac)
        except Exception as e:
            res['typ']= 'FAIL'
            res['Interaction']='FAIL'
            results.loc[getnum(FD_path)] = res
            fails.append(str(FD_path))
            continue
        
        #update the collected timestep
        res['timestep'] = np.diff(df['time'][:2])[0]*1E-9
        
        
        #set the fit object and try to fit
        transition_idx = np.argmax(hf.force > max_force)
        if transition_idx == 0:
            transition_idx = hf.loc[hf['force']==hf['force'].max()].index.values[0]
        rel = hf.loc[hf.index<transition_idx]
        fit = set_model(getnum(FD_path), rel['force'], rel['distance'])
        try:
            fit.fit()
        except:
            print('\n')
            print('did not converge!')
        
        #get the BF point
        bf = df.loc[df['type']=='interaction'].iloc[-1]        
        
        if not has_finish(df):
            res['problem'] = 'no finish'
        
        
        #now save the relevant information in the res dictionary
        if res['interaction']:
            res['BF'] = get_BF(df)
            res['OST'] = check_ost(df)
            res.update(get_params_from_fit(fit))
            #get the fitting quality
            force_model = getCurveFromModel(fit, rel['distance'].values)
            errs = deviation(rel['force'], force_model)
            res.update(errs)
        else:
            fit= None #reset the fit if decided to reject the interaction
        
        #check if single
        if check_single(df, res):
            res['type'] = 'single'
        else:
            res['type'] = 'multi'
        
        #save the results into the big dataframe
        results.loc[getnum(FD_path)] = res
        
        
        #now draw the lines
        fig, ax = plt.subplots()
        ax.plot(df.distance, df.force, 'o', markersize = 3, label='data', zorder = 7)
        ax.plot(hf.distance, hf.force, 'o', markersize = 3, alpha=0.4, zorder=2, label='HF_data')
        ax.plot(bf.distance, bf.force, color='red', marker= '^', linestyle='None', markersize = 5, label ='BF', zorder =7)
        draw_fit(fit, '--m', ax)
        
        #annotate on the BF
        x_point, y_point = bf.distance, bf.force
        ax.annotate(round(bf.force,2), xy=(x_point*1.02, y_point*1.02), 
                    #xytext=(x_point + 1, y_point + 10), #arrowprops=dict(arrowstyle='->')
                    )
        
        #adjust the plot:
        set_ax_settings(ax)
        ax.set_title(FD_path.name)
        if res['type']=='single':
            ax.patch.set_facecolor('green')
        else:
            if res['type']=='multi':
                ax.patch.set_facecolor('blue')
            else:
                ax.patch.set_facecolor('yellow')
        
        #alert for no finish:
        if not has_finish(df):
            res['type'] = 'undef'
            ax.patch.set_facecolor('maroon')
            ax.text(0.1, 175, 'No Finish!', ha='left', va='top', color='red', fontsize='x-large',)
        
        #add text near plot
        rounded_res = {key: str(round(value, 2)) for key, value in res.items() if type(value)==np.float64}
        tex = pd.DataFrame(list(rounded_res.items()), columns=['Key', 'Value']).set_index('Key').to_string()
        ax.text(0.92, 0.5, tex, fontsize=8, transform=fig.transFigure)  # Adjust the position (1, 1) and fontsize as needed
        #add the type
        ax.text(4.0, 0.1, res['type'], ha='center', fontsize = 'x-large')
        
        
        #finish plot buiseness- show, dpi and save
        #plt.show()
        #fig.set_api(400)
        fig.savefig(str(resfold_int/(FD_path.name.removesuffix('.h5')+'.png')), dpi=400, bbox_inches='tight')
        plt.close()
        
        
        
        #save the current curve
        if save_all or (save_interaction and res['interaction']):
            p = str(resfold_int/(FD_path.name.removesuffix('.h5')))
            info = pd.DataFrame([res]).T
            extract_curve(p, df, hf, fit=fit, info=info)
            
            
    #now iterate over the automatic non interacting ones
    #%matplotlib inline
    for FD_path in tqdm(nonts, desc=f'processing {len(nonts)} flat graphs'):
        
        FD_path = Path(FD_path)
        fit = None
        
        #generate result dictionary
        res = {key: None for key in collected_results}
        res['file_name'] = str(FD_path)
        res['interaction'] = False
        
        #try to open the file
        try:
            distance, force, time = read_forcefiles(FD_path, frq)
            df = pd.DataFrame(dict(time = time, distance = distance, force = force, group=-1, type='bottom'))
            #df = treat(df)
        
        except Exception as e:
            res['typ']= 'FAIL'
            res['Interaction']='FAIL'
            results.loc[getnum(FD_path)] = res
            fails.append(str(FD_path))
            continue
        
        #update the collected timestep
        res['timestep'] = np.diff(df['time'][:2])[0]*10E-9
        
        #fig, ax =type_draw(df, marker='o', linestyle='None', markersize=3)
        fig, ax = plt.subplots()
        ax.plot(df['distance'], df['force'], marker='o', linestyle='None', markersize=3, label=df.type.unique()[0])
        ax.patch.set_facecolor('grey')
        set_ax_settings(ax)
        ax.legend(fontsize='small')
        ax.set_title(FD_path.name)
        fig.set_dpi(400)
        #plt.show()
        plt.savefig(str(resfold_non/(FD_path.name.removesuffix('.h5')+'.png')), dpi=400, )
        remove_HFline(ax)
        plt.close()
        
        results.loc[getnum(FD_path)] = res
        
        #save the current curve
        if save_all or (save_interaction and res['interaction']):
            p = str(resfold_non/(FD_path.name.removesuffix('.h5')))
            info = pd.DataFrame([res]).T
            extract_curve(p, df, fit=fit, info=info)
        
    
    results = final_adjustments(results)
    results_int = results.loc[results['interaction']=='YES']
    results_non = results.loc[results['interaction']=='no']
    
    #save the results
    p = resfold/(f'{resfold.parts[-2]}_{resfold.parts[-1]}')
    p_int = resfold_int/(f'{resfold_int.parts[-3]}_{resfold_int.parts[-2]}_{resfold_int.parts[-1]}')
    p_non = resfold_non/(f'{resfold_non.parts[-3]}_{resfold_non.parts[-2]}_{resfold_non.parts[-1]}')
    
    process_n_save(results, p)
    process_n_save(results_int, p_int)
    process_n_save(results_non, p_non)    
    
    print('\n')
    
if len(fails)>0: print(f'THERE WERE {len(fails)} FAILS: \n', fails)
else: print('No curves failed')



#%%

#now plotting failures:
for FD_path in fails:
    plt.figure()
    try:
        FD_path = Path(FD_path)
        distance, force, time = read_forcefiles(FD_path, frq)
        df = pd.DataFrame(dict(time = time, distance = distance, force = force, group=-1, type=None))
        hf = read_HF(FD_path)
        plt.plot(df.distance, df.force)
        plt.plot(hf.distance, hf.force, zorder=0)
    except:
        None
    plt.gcf().suptitle(FD_path.name)
    plt.show()



