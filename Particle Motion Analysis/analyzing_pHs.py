# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 12:23:00 2024

@author: Alon Grossman
"""


import pandas as pd 

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc,rcParams

import numpy as np

import seaborn as sns 

from trackpy.utils import pandas_sort, pandas_concat, guess_pos_columns
import trackpy as tp

from operator import itemgetter
from cycler import cycler
from matplotlib.gridspec import GridSpec

from advanced_analysis_costume_functions import *

import glob 
import pathlib
import pims
from pathlib import Path
import matplotlib.patches as mpatches
import os
from matplotlib_scalebar.scalebar import ScaleBar

#%%
import textwrap
def wrap_labels(ax, width, break_long_words=False):
    labels = []
    for label in ax.get_xticklabels():
        text = label.get_text()
        labels.append(textwrap.fill(text, width=width,
                      break_long_words=break_long_words))
    ax.set_xticklabels(labels, rotation=0)
    
def signify(p_value):
    if p_value > 0.05:
        return 'ns'
    elif p_value <= 0.05 and p_value > 0.01:
        return '*'
    elif p_value <= 0.01 and p_value > 0.001:
        return '**'
    else:
        return '***'
    
#%% gather videos
mpl.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["font.size"] = 10
plt.rcParams["axes.labelweight"] = "bold"
#plt.rcParams["font.linespacing"] = 1
remove_stuck = False

#%%

#date_dirs = [r"E:\Alon\new analyses\for paper\pHs\2024_01_24_tracks",
 #            r"E:\Alon\new analyses\for paper\pHs\2024_01_29_tracks",
  #           r"E:\Alon\new analyses\for paper\pHs\2024_01_30_tracks",
   #          r"E:\Alon\new analyses\for paper\pHs\2024_02_01_tracks",
    #         r"E:\Alon\new analyses\for paper\pHs\2024_02_02_tracks",
     #        r"E:\Alon\new analyses\for paper\pHs\2024_02_05_tracks",
      #       ]

mdir = r"E:\Alon\new analyses\for paper\pHs"
videos = get_lowest_dirs(mdir)
#videos = flatten([get_lowest_dirs(d) for d  in date_dirs])
videos = [Path(v) for v in videos]
videos = videos[2::4] #take only the second video

#check x4 number of videos
if len(videos)%4 == 0 :
    print('4videos each')
    
#%% read info from info files

info = []
for video in videos:
    di = parse_info_file(video/(video.name + '_surface_info.txt'))
    di['vid_name'] = video.name
    di['vid_path'] = str(video)
    di['save_path'] = str(video/video.name)
    info.append(di)

info = pd.DataFrame(info)
info['vid_count'] = info.groupby(['date', 'surface']).cumcount()+1
#info = pd.concat([info['video'], info.drop('video', axis=1)], axis=1) #change video column to be first- doesnt mater at all.
info['label'] = 'pH ' + info['ph'] + ', ' + info['divalent'] + ', prot ' + info['prot']
info['label'] = 'pH ' + info['ph'] + ', ' + info['divalent']
info.loc[info['prot']=='non', 'label'] = 'no GPdTM'
info['legend'] = info['label'].apply(lambda s: s.replace('Ca', '${\mathrm{Ca}^{2+}}$'))
info['legend'] = info['legend'].apply(lambda s: s.replace('EDTA', '${\mathrm{EDTA}^{}}$'))
#info['legend'] = info['legend'].apply(lambda s: s.replace(',', '\n'))



#info['key'] = info['label']
#%% define video params
mpp = 0.21
fps = 33
max_lagtime= 2000 #should be N/10

#%% define anlysis params:
t0 = 30
R0 = 0.01
Rb = 1.3


#%% gather trajectories
l = []
for index, row in info.iterrows():
    traj = pd.read_csv(row['save_path']+"_final_track.csv")
    traj['particle'] += index*1000
    traj['vid_name'] = row['vid_name']
    
    traj = tp.filter_stubs(traj, 2500)
    
    print(f'vid {index}', row['vid_path'], row['label'])
    print (traj['particle'].nunique())
    
    l.append(traj)

traj_combi = pd.concat(l)
traj_combi = traj_combi.reset_index(drop=True)

#%% now add the info of each movie to trajectories
#traj_combi = pd.merge(traj_combi, info, on = 'vid_name', how='left')
#%% calculate msds
msds = tp.imsd(traj_combi, mpp, fps, max_lagtime)

#%% devide trajetories by motion
traj_combi = divide_traj_by_MSD(traj_combi, msds, t0, R0, Rb)

#filter out unassigned particle if they exist
msk = traj_combi['motion'].isnull()
if msk.sum() != 0:    
    n= traj_combi.loc[msk]['particle'].nunique()
    traj_combi = traj_combi.loc[~msk]
    print('# deleted particles: ',n )

#%% save the devided trajectories
#saveplace = r"E:\Alon\new analyses\devided.pkl"
#save_variables_to_file(saveplace, traj_combined)

#%% gather particle info
par_vids = traj_combi[['vid_name', 'particle', 'motion']].drop_duplicates()    #this assumes motion classification is done on the trajectories themselves
particle_info = pd.merge(par_vids, info, on = 'vid_name', how = 'left')

#add msd_t0:
msds_t0 = msds[msds.index==t0].T.reset_index().rename(columns= {'index': 'particle', t0 : 'msd_t0'})
particle_info = pd.merge(particle_info, msds_t0, on = 'particle', how='left')


#defining palette
pal = sns.color_palette('Paired', 6)
#pal = flatten([[pal[i+1], pal[i]] for i in range(0,len(pal),2)])
pal = pal[::-1]
pal[2:] = pal[4:6] + pal[2:4]
pal.insert(0, 'darkgrey')
#pal = [item[::-1] for item in pal[::-1]]


#%%


print('\n median msd: ')
print(particle_info.groupby('label')['msd_t0'].median())
print('\n means:')
print(particle_info.groupby('label')['msd_t0'].mean())

print('\n median msd of bound')
sub = particle_info[particle_info['motion']=='bound']
print(sub.groupby('label')['msd_t0'].median())
print('\n means:')
print(sub.groupby('label')['msd_t0'].mean())


#%% plots





#%% histogram to check your variables
plt.figure()
sns.histplot(particle_info, x='msd_t0', #hue='label', 
             stat = 'density', 
             log_scale=True, bins=100, element='bars', common_norm=False, kde=True, alpha = 0.4, palette = pal,#'magma',
             edgecolor='w', linewidth=0, multiple='layer',
             kde_kws = dict(bw_adjust = 0.2, gridsize=500), line_kws=dict(linewidth=2))

#sns.kdeplot(data, x='msd_t0', log_scale=True, bw_adjust = 0.4, gridsize=500, color='k', linestyle=':')

ax=plt.gca()
ax.axvline(x=Rb, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.axvline(x=R0, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.text(x=Rb, y = ax.get_ylim()[1]*1.02, s='$R_b$', rotation=0, horizontalalignment='center',  alpha=1,)
ax.text(x=R0, y = ax.get_ylim()[1]*1.02, s='$R_0$', rotation=0, horizontalalignment='center',  alpha=1)
#sns.move_legend(ax, 'lower center', bbox_to_anchor=(0.5,-0.7), frameon=False, ncol=1, title='', labels= labels.values() )
#sns.move_legend(ax, 'upper left', bbox_to_anchor=(1,1), frameon=False, ncol=1, title='', labels= labels.values() )
ax.set(xlabel = r'$\mathrm{MSD(t_0)}$')

plt.gcf().set_dpi(400)



#%% get MSDs of the two exetreme conditions
# define colors and labels
keys = ['pH 5.2, Ca', 'pH 7.5, EDTA']#, 'pH 5.2, EDTA', 'pH 7.5, Ca']
keys = ['5.2','7.5']#, 'pH 5.2, EDTA', 'pH 7.5, Ca']


colors = ['r', 'b']#, 'y', 'c']
colors = {key: colors[i] for i, key in enumerate(keys)}

#labels = [r'pH 5.2 + ${\mathrm{Ca}^{2+}}$', r'pH 7.5 + EDTA']
labels= keys
labels = {key: 'pH '+ labels[i] for i, key in enumerate(keys)}

medcolors = ['darkred', 'midnightblue']#, 'y', 'c']
medcolors = {key: medcolors[i] for i, key in enumerate(keys)}


fig, ax = plt.subplots()
fig.set_size_inches(5,4)

for i, key in enumerate(keys):
    #pars = particle_info[particle_info['label'] == key]['particle']
    pars = particle_info[(particle_info['ph'] == key) & (particle_info['prot'] != 'non')]['particle']
    ms = msds[pars]
    ms = ms[ms.columns[::6]]
    
    ax.plot(ms.index, ms, alpha = 0.15, label = key, color = colors[key],)
    _, med, _ = get_median_particle(ms, t0)
    ax.plot(med.index, med, '--', alpha=1, label=key, color=medcolors[key], linewidth=2, zorder=10)


ax.set(yscale='log',
       xscale='log',
       #xlim = (0.065, None),
       #ylim = (5e-5, 217)
       )
ax.set_xlabel(r'lag time $\tau$ [s]', fontsize=12)
ax.set_ylabel(r'MSD($\tau$) [$\mathrm{\mu m^2}$]', fontsize=12)
ax.axhline(y=Rb, color='k',linewidth=0.5, alpha = 0.5)
ax.axhline(y=R0, color='k',linewidth=0.5, alpha = 0.5)
ax.axvline(x=t0, color='k',linewidth=0.5, alpha = 0.5)

ax.text(y=Rb, x = ax.get_xlim()[1]*1.06, s=f'$R_b$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(y=R0, x = ax.get_xlim()[1]*1.06, s=f'$R_0$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(x=t0, y = ax.get_ylim()[1]*1.4, s=f'$t_0$', rotation=0, horizontalalignment='center',  alpha=0.85)

#ax.patch.set_alpha(0)

from matplotlib.lines import Line2D

handles = [Line2D([0], [0], color=colors[key], label=labels[key]) for key in reversed(keys)]
ax.legend(handles = handles, frameon=False, fontsize = 12)

#fig.suptitle('time average MSD', size=15)
#plt.tight_layout()
fig.set_dpi(400)
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 1\msds_5_v_7.pdf",  bbox_inches = 'tight', transparent=True)

#%% now bar plot 

#%% for percentages remove stuck particles 
particle_info_f = particle_info[particle_info['motion']!='stuck'].copy()
#%% calculate frequencies of the different groups in each movie
l = []
for vid_name, group in particle_info_f.groupby('vid_name'):
    df = traj_group_freqs(group)
    df['vid_name'] = vid_name
    df['motion'] = df.index
    
    df['legend'] = group['legend'].iloc[0]
    df['label'] = group['label'].iloc[0]
    df['date'] = group['date'].iloc[0]
    df['ph'] = group['ph'].iloc[0]
    df['divalent'] = group['divalent'].iloc[0]
    df['prot'] = group['prot'].iloc[0]
    l.append(df)

res = pd.concat(l)

filt = res[res['motion'] == 'bound']

#%% mann whitney all
from scipy.stats import mannwhitneyu
vals = filt[['freq', 'ph', 'prot', 'divalent', 'label', 'motion']].reset_index(drop=True)

vals_labels = {label: group['freq'].values for label, group in vals.groupby('label')}
Ps = pd.DataFrame(columns = vals_labels.keys(), index = vals_labels.keys())
for label1 in vals_labels:
    for label2 in vals_labels:
        U1, p = mannwhitneyu(vals_labels[label1], vals_labels[label2], method="exact")
        Ps.loc[label1, label2] = p
print(Ps)

#%% bar plot
order = filt['label'].unique()
order.sort()
order = np.roll(order, -1)

fig, ax = plt.subplots()
fig.set_size_inches((6,4))
#start with the bar plot
sns.barplot(data=filt, y='freq', x='label', hue='label', palette=pal, order = order, 
            #hue_order = order,#order[1:] + order[:1],
            edgecolor = "0", linewidth = 1.5,
            errorbar='se', capsize = 0.2, errwidth = 1.5, errcolor = ".4",
            width = 0.8,
            )

#add data point
sns.stripplot(data=filt, y='freq', x='label', 
              color='k', alpha =0.7, #size=5,
              #color='w', alpha =1, edgecolor='k', linewidth=1,
              #palette=pal, alpha =1, edgecolor='k', linewidth=1,
              #jitter=False,
              ax=ax)

ax.set(ylim = (0,102))

#significance
ref_group = 'no GPdTM'
sig_signs = Ps.applymap(signify)
for cond, sig in sig_signs[ref_group].items():
    #print(cond, sig)
    if not cond==ref_group:
        if sig == 'ns':
            siz = 14
            y_factor = 1.05
        else:
            siz = 18
            y_factor = 1.025
        ax.text(x=cond, y=ax.get_ylim()[1]*y_factor, s=sig, ha='center', va='center', size = siz)
        ax.text(x=cond, y=ax.get_ylim()[1]*1.05, s='___', ha='center', va='center', size = 16, weight='normal')


sns.despine()

ax.set(xlabel='',
       ylabel = 'Fraction of Bonud Particles (%)')
plt.yticks(size=12)
wrap_labels(ax, 7)
plt.gcf().set_dpi(400)
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 2\bound_fraction_phs.pdf",  bbox_inches = 'tight')
#ax.set_xticklabels('')
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 2\bound_fraction_phs_no_labels.pdf",  bbox_inches = 'tight', transparent=True)


#%% calc percentage errors
avs = filt[['motion', 'freq', 'label']].groupby(['label','motion']).mean()
ers = filt[['motion', 'freq', 'label']].groupby(['label','motion']).sem()
nums = res.groupby('label')['num'].sum()

print('\n percentages +- SEM: ')
print((avs.round(2).astype(str) + ' +-' + ers.round(2).astype(str)))
print('\n')


#pd.concat([avs, ers,nums], axis=1)
#%% calculate p values significance by mann whitney 
vals_prot = vals[vals['prot'] != 'non']

#first compare Ca/EDTA in each pH- they all look nonsignificant
print('\n mann-whitney for Ca/EDTA whithin phs:')
for ph, gr in vals_prot.groupby('ph'):
    l = [sub['freq'].values for div, sub in gr.groupby('divalent')]
    #v_ca = gr[gr['divalent']=='Ca']['freq']
    #v_ed = gr[gr['divalent']=='EDTA']['freq']
    #U1, p = mannwhitneyu(v_ca, v_ed, method="exact")
    U1, p = mannwhitneyu(l[0], l[1], method="exact")
    print(ph, U1, p)
    
#%% now compare the different pHs to one another
vals_phs = {ph: group['freq'].values for ph, group in vals_prot.groupby('ph')}
print('\n mann-whitney for phs:')
for ph1 in vals_phs:
    for ph2 in vals_phs:
        U1, p = mannwhitneyu(vals_phs[ph1], vals_phs[ph2], method="exact")
        print(f'pH {ph1} to {ph2}', p)



#%% ratio between bound and stuck
particle_info_r = particle_info.copy()
#%calculate frequencies of the different groups in each movie
l = []
for vid_name, group in particle_info_r.groupby('vid_name'):
    df = traj_group_freqs(group)
    df['vid_name'] = vid_name
    df['motion'] = df.index
    
    df['legend'] = group['legend'].iloc[0]
    df['label'] = group['label'].iloc[0]
    df['date'] = group['date'].iloc[0]
    df['ph'] = group['ph'].iloc[0]
    df['divalent'] = group['divalent'].iloc[0]
    df['prot'] = group['prot'].iloc[0]
    l.append(df)

res2 = pd.concat(l)
res2.reset_index(inplace=True)

#filt = res[res['motion'] == 'bound']


#%% plot the freqs of all motion groups
fig, ax = plt.subplots()
sns.barplot(data=res2, y='freq', x='label', hue='motion', palette=pal, #order = order, 
            #hue_order = order,#order[1:] + order[:1],
            errorbar='se', ax=ax,
            linewidth = 1, edgecolor="0", saturation = 0.75,
            )
wrap_labels(ax, 7)
fig.set_dpi(200)

#%% calc the ratio between frequencies of stuck and bound
# Group by 'vid_name' and select frequencies for bound and stuck
all_freqs = res2.pivot_table(index=['vid_name', 'label', 'ph', 'divalent'], 
                             columns=['motion'], values='freq', aggfunc='max', 
                             fill_value=0)
all_freqs = all_freqs.reset_index()
# Calculate the ratio between bound and stuck particles
all_freqs['ratio_stuck_bound'] = all_freqs['stuck'] / all_freqs['bound']

#%%plot the ratio
fig, ax = plt.subplots()
sns.barplot(data=all_freqs, x='label', y='ratio_stuck_bound', hue='label', palette=pal, 
            order=order,
            errorbar='se', ax=ax,
            )
wrap_labels(ax, 7)
fig.set_dpi(200)

#%%


#%% now plot a smaller bar plot for figure 1 
filt2 = filt[(filt['prot'] != 'non') & (filt['ph']!='6.3')]

#filt2 = filt[(filt['label'].isin(['pH 5.2, Ca', 'pH 7.5, EDTA']))]  # to use this change to x='label' in the plotting functions

order = filt2['ph'].unique()
order.sort()
pal2 = [pal[1]] + [pal[3]] # + [pal[5]]
pal2 = ['r', 'b'] # + [pal[5]]
pal2 = [(0.9,0,0), (0,0,0.88)]

fig, ax = plt.subplots()
fig.set_size_inches(2,4)
sns.barplot(data=filt2, y='freq', x='ph', hue='ph', palette=pal2, order = order, 
            #hue_order = order,#order[1:] + order[:1],
            saturation=0.9,
            edgecolor = "0", linewidth = 1., width=0.6,
            errorbar='se', capsize = 0.2, errwidth = 1.5, errcolor = "0",
            ax=ax,
            )
sns.stripplot(data=filt2, y='freq', x='ph', 
              #color='k', alpha =0.7,
              color='w', alpha =1, edgecolor='k', linewidth=1, size=4,
              #palette=pal2, alpha =1, edgecolor='k', linewidth=1, 
              #jitter=False,
              ax=ax)
#sns.despine()
ax.set(ylim = (0,102))
ax.set_ylabel('Fraction of Bonud Particles (%)', size=12)
ax.set_xlabel('', size=12)
ax.set_xticklabels(['pH 5.2', 'pH 7.5'], size=12)
plt.yticks(fontsize=12)
wrap_labels(ax, 7)
#sns.despine()
plt.gcf().set_dpi(400)
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 1\bound_fraction_5_v_7.pdf",  bbox_inches = 'tight', transparent=True)



#%% plot example rajectories of the the groups

#%% get the particles particles
midpars = []
for group, subinfo in particle_info.groupby('motion'):
    midpar = subinfo.loc[subinfo['msd_t0'] == subinfo['msd_t0'].quantile(0.42, 'nearest')] #47 if you use cutlength of 1800
    midpars.append(midpar)
midpars = pd.concat(midpars)

#%
groups = ['free', 'bound', 'stuck']

ids = midpars['particle']
extrajs = traj_combi[traj_combi['particle'].isin(ids)]
extrajs.sort_values(by='motion', key = lambda s: s.map({'free': 0, 'bound': 1, 'stuck': 2}))

fig, axs = plt.subplots(len(groups), )#sharex=True, sharey=True)
fig.set_size_inches(2,6)

for num, group in enumerate(groups):
    ax = axs[num]
    
    partraj = extrajs[extrajs['motion']==group]
    partraj[['y','x']] -=  partraj[['y','x']].mean()
    partraj[['y','x']] *=  mpp
    
    tp.plot_traj(partraj, colorby='frame', ax=ax)
    
    if group == 'free' or group == 'bound':
        lims = np.array(ax.get_xlim(), ax.get_ylim())
        m = round(max(abs(lims)))+0.5
    
    ax.set(
        aspect='equal',
        #adjustable='box',
        #xlabel=r'$\mathrm{\mu m}$',
        xlabel=r'',
        #ylabel=r'$\mathrm{\mu m}$',
        ylabel=r'',
        xlim =(-m, m),
        ylim =(-m, m),
        #title = group.capitalize()
        )
    ax.set_title( group.capitalize(),y=0.9, x=0.04, ha='left', va = 'top', fontsize=14, fontweight="bold")
    
    ax.tick_params(left = False, right = False , labelleft = False , 
            labelbottom = False, bottom = False) 
    scalebar = ScaleBar(1, 'um', scale_loc='top', length_fraction=0.5, location = 'lower right' )
    ax.add_artist(scalebar)

axs[2].sharex(axs[1])
axs[2].sharey(axs[1])
#ax.yaxis.set_ticks(ax.xaxis.get_ticklocs())
#fig.suptitle(labels[key])
#fig.supxlabel(r'$\mathrm{\mu m}$')
#fig.supylabel(r'$\mathrm{\mu m}$')
fig.subplots_adjust(wspace=0.4, hspace=0.05 ,bottom =0.1 )
fig.set_dpi(500)
plt.show()
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 1\example trajectories.pdf",  bbox_inches = 'tight', transparent=True)



'''
#%% now try to plot msd_t0 distribution
filt3 = particle_info[particle_info['motion']!='free']
filt3 = filt3[filt3['prot']!='non']

fig, ax = plt.subplots()
sns.violinplot(data=filt3, y='msd_t0', x='ph', order = ['5.2', '6.3', '7.5'], hue='divalent', split=True, bw_adjust=0.5)
#sns.boxplot(data=filt3, y='msd_t0', x='label', order=order[:-1], ax=ax)
wrap_labels(ax, 7)
fig.set_dpi(200)
#%%
fig, ax = plt.subplots()
sns.violinplot(data=filt3, y='msd_t0', x='ph', order = ['5.2', '6.3', '7.5'], bw_adjust=0.5)
#sns.boxplot(data=filt3, y='msd_t0', x='label', order=order[:-1], ax=ax)
wrap_labels(ax, 7)
fig.set_dpi(400)

#%%
fig, ax = plt.subplots()
sns.boxplot(data=filt3, y='msd_t0', x='ph', order = ['5.2', '6.3', '7.5'], )
#sns.boxplot(data=filt3, y='msd_t0', x='label', order=order[:-1], ax=ax)
wrap_labels(ax, 7)
fig.set_dpi(400)

#%%
fig, ax = plt.subplots()
#sns.violinplot(data=filt3, y='msd_t0', x='ph', order = ['5.2', '6.3', '7.5'], bw_adjust=0.5)
sns.boxplot(data=filt3, y='msd_t0', x='ph', hue = 'divalent', order = ['5.2', '6.3', '7.5'], ax=ax)
wrap_labels(ax, 7)
fig.set_dpi(400)

#%%
fig, ax = plt.subplots()
#sns.violinplot(data=filt3, y='msd_t0', x='ph', order = ['5.2', '6.3', '7.5'], bw_adjust=0.5)
sns.pointplot(data=filt3, y='msd_t0', x='ph', hue = 'divalent', order = ['5.2', '6.3', '7.5'], ax=ax)
wrap_labels(ax, 7)
fig.set_dpi(400)
'''