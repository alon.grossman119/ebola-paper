# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 22:31:00 2024

@author: Alon Grossman
"""

#%% 

import pandas as pd 

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc,rcParams

import numpy as np

import seaborn as sns 

from trackpy.utils import pandas_sort, pandas_concat, guess_pos_columns
import trackpy as tp

from operator import itemgetter
from cycler import cycler
from matplotlib.gridspec import GridSpec

from advanced_analysis_costume_functions import *

import glob 
import pathlib
import pims
from pathlib import Path
import matplotlib.patches as mpatches
import os

#%%
import textwrap
def wrap_labels(ax, width, break_long_words=False):
    labels = []
    for label in ax.get_xticklabels():
        text = label.get_text()
        labels.append(textwrap.fill(text, width=width,
                      break_long_words=break_long_words))
    ax.set_xticklabels(labels, rotation=0)
    
def signify(p_value):
    if p_value > 0.05:
        return 'ns'
    elif p_value <= 0.05 and p_value > 0.01:
        return '*'
    elif p_value <= 0.01 and p_value > 0.001:
        return '**'
    else:
        return '***'
#%% gather videos
mpl.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["font.size"] = 10
plt.rcParams["axes.labelweight"] = "bold"
#plt.rcParams["font.linespacing"] = 1
remove_stuck = False

#%%
mdir = r"E:\Alon\new analyses\for paper\receptor"
videos = get_lowest_dirs(mdir)
#videos = flatten([get_lowest_dirs(d) for d  in date_dirs])
videos = [Path(v) for v in videos]
videos = videos[2::4] #take only the second video

#check x4 number of videos
if len(videos)%4 == 0 :
    print('4videos each')
    
#%% read info from info files

info = []
for video in videos:
    di = parse_info_file(video/(video.name + '_surface_info.txt'))
    di['vid_name'] = video.name
    di['vid_path'] = str(video)
    di['save_path'] = str(video/video.name)
    info.append(di)

info = pd.DataFrame(info)
info['vid_count'] = info.groupby(['date', 'surface']).cumcount()+1
info['prot'] = info['prot'].apply(lambda s: '' if s=='no' else 'GPdTM')
info['receptor'] = info['receptor'].apply(lambda s: '' if s=='no' else 'sNPC1')
info['label'] = info['prot'] + ' ' + info['receptor']
info['label'] = info['label'].apply(lambda s: s.strip())
info.loc[info['label']=='', 'label'] = 'no GPdTM'
info = info[~info['label'].isin(['sNPC1', 'no GPdTM'])]
#%% define video params
mpp = 0.21
fps = 33
max_lagtime= 2000 #should be N/10

#%% define anlysis params:
t0 = 30
R0 = 0.01
Rb = 1.3


#%% gather trajectories
l = []
for index, row in info.iterrows():
    traj = pd.read_csv(row['save_path']+"_final_track.csv")
    traj['particle'] += index*1000
    traj['vid_name'] = row['vid_name']
    
    traj = tp.filter_stubs(traj, 2500)
    
    print(f'vid {index}', row['vid_path'], row['label'])
    print (traj['particle'].nunique())
    
    l.append(traj)

traj_combi = pd.concat(l)
traj_combi = traj_combi.reset_index(drop=True)

#%% now add the info of each movie to trajectories
#traj_combi = pd.merge(traj_combi, info, on = 'vid_name', how='left')
#%% calculate msds
msds = tp.imsd(traj_combi, mpp, fps, max_lagtime)

#%% devide trajetories by motion
traj_combi = divide_traj_by_MSD(traj_combi, msds, t0, R0, Rb)

#filter out unassigned particle if they exist
msk = traj_combi['motion'].isnull()
if msk.sum() != 0:    
    n= traj_combi.loc[msk]['particle'].nunique()
    traj_combi = traj_combined[key].loc[~msk]
    print('# deleted particles: ',n )

#%% save the devided trajectories
#saveplace = r"E:\Alon\new analyses\devided.pkl"
#save_variables_to_file(saveplace, traj_combined)

#%% gather particle info
par_vids = traj_combi[['vid_name', 'particle', 'motion']].drop_duplicates()    #this assumes motion classification is done on the trajectories themselves
particle_info = pd.merge(par_vids, info, on = 'vid_name', how = 'left')

#add msd_t0:
msds_t0 = msds[msds.index==t0].T.reset_index().rename(columns= {'index': 'particle', t0 : 'msd_t0'})
particle_info = pd.merge(particle_info, msds_t0, on = 'particle', how='left')


#%%defining palette
pal = sns.color_palette('Paired', 8)
#pal = flatten([[pal[i+1], pal[i]] for i in range(0,len(pal),2)])
pal = pal[-2:]
pal = pal[::-1]
#pal[2:] = pal[4:6] + pal[2:4]
#pal.insert(0, 'darkgrey')
#pal = [item[::-1] for item in pal[::-1]]

#%% plots
#%% for percentages remove stuck particles 
particle_info_f = particle_info[particle_info['motion']!='stuck'].copy()
#%% calculate frequencies of the different groups in each movie
l = []
for vid_name, group in particle_info_f.groupby('vid_name'):
    df = traj_group_freqs(group)
    df['vid_name'] = vid_name
    df['motion'] = df.index
    
    #df['legend'] = group['legend'].iloc[0]
    df['label'] = group['label'].iloc[0]
    df['date'] = group['date'].iloc[0]
    df['prot'] = group['prot'].iloc[0]
    df['receptor'] = group['receptor'].iloc[0]
    l.append(df)

res = pd.concat(l)

filt = res[res['motion'] == 'bound']

#%% calc percentage errors
avs = filt[['motion', 'freq', 'label']].groupby(['label','motion']).mean()
ers = filt[['motion', 'freq', 'label']].groupby(['label','motion']).sem()

print('\n percentages +- SEM: ')
print((avs.round(2).astype(str) + ' +-' + ers.round(2).astype(str)))
print('\n')


#%% calculate p values significance by mann whitney 
from scipy.stats import mannwhitneyu
vals = filt[['freq', 'prot', 'receptor', 'label', 'motion']].reset_index(drop=True)

vals_labels = {label: group['freq'].values for label, group in vals.groupby('label')}
Ps = pd.DataFrame(columns = vals_labels.keys(), index = vals_labels.keys())
for label1 in vals_labels:
    for label2 in vals_labels:
        U1, p = mannwhitneyu(vals_labels[label1], vals_labels[label2], method="exact")
        Ps.loc[label1, label2] = p

print(Ps)

#%% bar plot
order = filt['label'].unique()
#order.sort()
#order = np.roll(order, -1)
fig, ax = plt.subplots()
fig.set_size_inches((1.72,4))

#bars
sns.barplot(data=filt, y='freq', x='label', hue='label', palette=pal, order = order, 
            #hue_order = order,#order[1:] + order[:1],
            edgecolor = "0", linewidth = 1.5,
            errorbar='se', capsize = 0.2, errwidth = 1.5, errcolor = ".4",
            width = 0.8
            )

#data points
sns.stripplot(data=filt, y='freq', x='label', 
              color='k', alpha =0.6,
              #color='w', alpha =1, edgecolor='k', linewidth=1,
              #palette=pal, alpha =1, edgecolor='k', linewidth=1, 
              #jitter=False,
              ax=ax)

ax.set(ylim = (0,102))
#significance
ref_group = 'GPdTM'
sig_signs = Ps.applymap(signify)
for cond, sig in sig_signs[ref_group].items():
    #print(cond, sig)
    if not cond==ref_group:
        if sig == 'ns':
            siz = 14
            y_factor = 1.05
        else:
            siz = 18
            y_factor = 1.025
        ax.text(x=cond, y=ax.get_ylim()[1]*y_factor, s=sig, ha='center', va='center', size = siz)
        ax.text(x=cond, y=ax.get_ylim()[1]*1.05, s='___', ha='center', va='center', size = 16, weight='normal')

sns.despine()
ax.set(xlabel='',
       ylabel = 'Fraction of Bound Particles (%)')
plt.yticks(size=12)
wrap_labels(ax, 7)
plt.gcf().set_dpi(400)
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 2\bound_fraction_receptor.pdf",  bbox_inches = 'tight')
ax.set_xticklabels('')
plt.savefig(r"E:\Alon\new analyses\for paper\figures\Figure 2\bound_fraction_receptor_no_labels.pdf",  bbox_inches = 'tight', transparent=True)



#%%






#%% histogram to check your variables
'''
plt.figure()
sns.histplot(particle_info, x='msd_t0', hue='label', 
             stat = 'density', 
             log_scale=True, bins=40, element='bars', common_norm=False, kde=True, alpha = 0.4, palette = pal,#'magma',
             edgecolor='w', linewidth=0, multiple='layer',
             kde_kws = dict(bw_adjust = 0.4, gridsize=500), line_kws=dict(linewidth=2))

#sns.kdeplot(data, x='msd_t0', log_scale=True, bw_adjust = 0.4, gridsize=500, color='k', linestyle=':')

ax=plt.gca()
ax.axvline(x=Rb, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.axvline(x=R0, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.text(x=Rb, y = ax.get_ylim()[1]*1.02, s='$R_b$', rotation=0, horizontalalignment='center',  alpha=1,)
ax.text(x=R0, y = ax.get_ylim()[1]*1.02, s='$R_0$', rotation=0, horizontalalignment='center',  alpha=1)
#sns.move_legend(ax, 'lower center', bbox_to_anchor=(0.5,-0.7), frameon=False, ncol=1, title='', labels= labels.values() )
#sns.move_legend(ax, 'upper left', bbox_to_anchor=(1,1), frameon=False, ncol=1, title='', labels= labels.values() )
ax.set(xlabel = r'$\mathrm{MSD(t_0)}$')

plt.gcf().set_dpi(400)
'''


#%% get MSDs of the two exetreme conditions
# define colors and labels
'''
keys = info['label'].unique()


colors = ['c', 'y', 'g', 'k']#, 'y', 'c']
colors = {key: colors[i] for i, key in enumerate(keys)}

#labels = [r'pH 5.2 + ${\mathrm{Ca}^{2+}}$', r'pH 7.5 + EDTA']
labels= keys
labels = {key: labels[i] for i, key in enumerate(keys)}

medcolors = ['c', 'y', 'g', 'k']
medcolors = {key: medcolors[i] for i, key in enumerate(keys)}


fig, ax = plt.subplots()
fig.set_size_inches(5,4)

for i, key in enumerate(keys):
    pars = particle_info[particle_info['label'] == key]['particle']
    ms = msds[pars]
    ms = ms[ms.columns[::2]]
    
    ax.plot(ms.index, ms, alpha = 0.1, label = key, color = colors[key] )
    _, med, _ = get_median_particle(ms, t0)
    ax.plot(med.index, med, '--', alpha=1, label=key, color=medcolors[key], linewidth=2, zorder=10)


ax.set(#ylabel=r'$\langle \Delta r^2 \rangle$ [$\mathrm{\mu m^2}$]',
       ylabel= r'MSD($\tau$) [$\mathrm{\mu m^2}$]',
       xlabel=r'lag time $\tau$ [s]',
       yscale='log',
       xscale='log',
       #xlim = (0.065, None),
       ylim = (0.5e-4, 217)
       )


ax.axhline(y=Rb, color='k',linewidth=0.5, alpha = 0.5)
ax.axhline(y=R0, color='k',linewidth=0.5, alpha = 0.5)
ax.axvline(x=t0, color='k',linewidth=0.5, alpha = 0.5)

ax.text(y=Rb, x = ax.get_xlim()[1]*1.06, s=f'$R_b$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(y=R0, x = ax.get_xlim()[1]*1.06, s=f'$R_0$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(x=t0, y = ax.get_ylim()[1]*1.4, s=f'$t_0$', rotation=0, horizontalalignment='center',  alpha=0.85)

#ax.patch.set_alpha(0)

from matplotlib.lines import Line2D

handles = [Line2D([0], [0], color=colors[key], label=labels[key]) for key in reversed(keys)]
ax.legend(handles = handles, frameon=False)

#fig.suptitle('time average MSD', size=15)
#plt.tight_layout()
fig.set_dpi(400)
'''
#%% now bar plot 