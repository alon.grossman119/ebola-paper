# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 15:31:23 2023

@author: Alon Grossman
"""

import pandas as pd 

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc,rcParams

import numpy as np

import seaborn as sns 

from trackpy.utils import pandas_sort, pandas_concat, guess_pos_columns
import trackpy as tp

from operator import itemgetter
from cycler import cycler
from matplotlib.gridspec import GridSpec

from advanced_analysis_costume_functions import *

import glob 
import pathlib
import pims
from pathlib import Path
import matplotlib.patches as mpatches
import os

#%%
mpl.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["font.size"] = 14
plt.rcParams["axes.labelweight"] = "bold"

remove_stuck = True

#%% gather dirs in pH = 5.2


non = get_lowest_dirs(r"E:\Alon\new analyses\for paper\control\no protein")

strep = get_lowest_dirs(r"E:\Alon\new analyses\for paper\control\bio-strep")

prot = get_lowest_dirs(r"E:\Alon\new analyses\for paper\control\GPdTM")

save_path = non + strep + prot
exp_type = ['non']*len(non) + ['strep']*len(strep) + ['prot']*len(prot)

binning = [False]*len(non) + [False] * len(strep) + [False, True, True, True]

filename = [Path(d).name for d in save_path]

to_ignore=[]

special = [False]*(len(non) + len(strep)) + [False, True, False, True]

keys = list(set(exp_type))
keys.sort()

#%%
colors = ['orange', 'red', 'green']
colors = {key: colors[i] for i, key in enumerate(keys)}
labels = ['no protein', 'GPdTM', 'bio- strep']
labels = {key: labels[i] for i, key in enumerate(keys)}

medcolors = {key: 'dark'+colors[key] for key in colors} 

#%% define video params
mpp = 0.21
fps = 10
max_lagtime= 600 #should be N/10

#%% define anlysis params:
t0 = 30
R0 = 0.006
Rb = 1.3

#%% now gather all trajectories
cutframes = 1200
fullength = 6000
i = 0 
construct= pd.read_csv(save_path[i]+'\\'+filename[i]+"_final_track.csv")

traj_combined = {item:construct[0:0] for item in set(exp_type)}

s = 0
for i, path in enumerate(save_path):
    if i in to_ignore: continue
    traj = pd.read_csv(save_path[i]+'\\'+filename[i]+"_final_track.csv")
    traj['particle'] += i*10000
    p = pathlib.Path(save_path[i])
    traj['date'] = p.parts[-3].removesuffix('_track')
    traj[['folder', 'name']] = p.parts[-2:]
    traj['key'] = exp_type[i]
    traj = tp.filter_stubs(traj, 2500)
    
    if special[i]:
        traj = traj[traj['frame']<cutframes]
    else:    
        traj = traj[traj['frame']>(fullength-cutframes)]
    
    if not binning[i]: #take care of binning yourself so that all experiments have the same mpp.
        traj['x'] /= 2
        traj['y'] /= 2 
    
    print(save_path[i], exp_type[i], f'{i=}')
    #print(exp_type[i])
    print (traj['particle'].nunique())
    #print(len(traj['frame'].unique()))
    s += traj['particle'].nunique()
    traj_combined[exp_type[i]] = pd.concat([traj_combined[exp_type[i]], traj])

#%% calculate msds

msds = {key: tp.imsd(traj_combined[key], mpp, fps, max_lagtime) for key in keys}

#%% now devide the trajs נט צםאןםמ
for key in traj_combined:
    msd_t = tp.imsd(traj_combined[key], mpp, fps, max_lagtime)
    traj_combined[key] = divide_traj_by_MSD(traj_combined[key], msd_t, t0, R0, Rb)
    #filter out unassigned particle if they exist
    msk = traj_combined[key]['motion'].isnull()
    if msk.sum() != 0:    
        n= traj_combined[key].loc[msk]['particle'].nunique()
        traj_combined[key] = traj_combined[key].loc[~msk]
        print(key, ': # deleted particles: ',n )

#%% you can now save the traj to a file if you want 
saveplace = r"E:\Alon\data for paper\data\by category\control\sup_traj.pkl"
save_variables_to_file(saveplace, traj_combined)
#%% gather particle info
particle_info= {}
for key in keys:
    particle_info[key] = traj_combined[key].groupby('particle').agg({'name': 'first', 'key': 'first', 'motion': 'first'}).reset_index()
    particle_info[key].set_index('particle', inplace=True)

#add msds at t0 
msds_t0 = {}
for key in keys:
    msds_t0[key] = msds[key][msds[key].index==t0].T
    particle_info[key]['msd_t0'] = msds_t0[key]

# mold it to a dataframe
data = pd.DataFrame()
for key in keys :
    data = pd.concat([data, particle_info[key]])

#data.to_csv(r"E:\Alon\data for paper\data\by category\control\sup_data.csv")
#%% if easked, remove the stuck particle before calculatin the bar plots
if remove_stuck:    
    for key in traj_combined :
        temp = traj_combined[key]
        traj_combined[key] = temp.loc[temp['motion']!='stuck']
    keys = list(traj_combined.keys())
    keys.sort()

#%% calculate frequencies of the different groups
dic = {}
for key in traj_combined:
    dic[key] = traj_group_freqs(traj_combined[key])

frequencies = pd.concat([dic[key]['freq'] for key in keys], axis=1)
frequencies.columns = keys
frequencies = frequencies.iloc[::-1]

#%% getting error bars for frequencies
res = pd.DataFrame()
for key in traj_combined:
    traj = traj_combined[key]
    movies = traj['name'].unique()
    for movie in movies:
        df = traj_group_freqs(traj.loc[traj['name']==movie])
        df['movie'] = movie
        df['key'] = key
        df['motion'] = df.index
        res = pd.concat([res, df])

ers = res[['motion', 'freq', 'key']].groupby(['key','motion']).sem()
ers = ers.reset_index()
ers = ers.pivot_table(index='motion', columns='key', values='freq', aggfunc='sum')
#ers.reset_index(inplace=True)
ers.columns.name = None
ers.index.name = None
ers = ers.reindex(frequencies.index)

#%%
print('\n percentages +- SEM: ')
print((frequencies.round(2).astype(str) + ' +-' + ers.round(2).astype(str)).T)
print('\n')



#%% define groups
groups =['free', 'bound', 'stuck']
if remove_stuck:
    groups = ['free', 'bound']

#%% restrture the datafrom res to another dataframe for p test
# Pivot the DataFrame
pivot_res = res.pivot(index=['movie','key'], columns='motion', values=['num', 'freq'])
pivot_res.fillna(0, inplace=True)
pivot_res.reset_index(inplace=True)



#%% now run mann-whitney test- copmairing both prot and strep to non
print ('Mann-Whitney U test: \n')
from scipy.stats import mannwhitneyu
refkey = 'non'
pivots = {key: pivot_res[pivot_res['key']==key] for key in keys}


for key in pivots:
    print(key+":")
    for group in ['bound']:#groups:  
        ref_freqs = pivots[refkey]['freq'][group]
        check_freqs = pivots[key]['freq'][group]
        U1, p = mannwhitneyu(ref_freqs, check_freqs, method="exact")
        print(group, U1, p)

    
#%%%
print('\n median msd(t0) estimation: \n')
for key in keys:
    curr = data.loc[data.key == key]
    median, err = stat_CI(curr['msd_t0'], 0.95, np.median, 50000)
    print(key+': ', f'{round(median,3)} +- {round(err,3)}')
    print('IQR: ', round(curr['msd_t0'].quantile(0.25), 3), ',', round(curr['msd_t0'].quantile(0.75), 3))
#%%






#%% now for the plots!
#%% plot a histogram to check the validity of your parameters (based on msds)  -  with seaborn

plt.figure()
sns.histplot(data, x='msd_t0', hue='key', stat = 'density', 
             log_scale=True, bins=60, element='bars', common_norm=False, kde=True, alpha = 0.4, palette = colors,#'magma',
             edgecolor='w', linewidth=0, multiple='layer',
             kde_kws = dict(bw_adjust = 0.4, gridsize=500), line_kws=dict(linewidth=2))

#sns.kdeplot(data, x='msd_t0', log_scale=True, bw_adjust = 0.4, gridsize=500, color='k', linestyle=':')

ax=plt.gca()
ax.axvline(x=Rb, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.axvline(x=R0, color = 'k', linestyle = '--', linewidth=1.5, alpha = 0.9)
ax.text(x=Rb, y = ax.get_ylim()[1]*1.02, s='$R_b$', rotation=0, horizontalalignment='center',  alpha=1,)
ax.text(x=R0, y = ax.get_ylim()[1]*1.02, s='$R_0$', rotation=0, horizontalalignment='center',  alpha=1)
sns.move_legend(ax, 'lower center', bbox_to_anchor=(0.5,-0.4), frameon=False, ncol=3, title='', labels= labels.values() )
#sns.move_legend(ax, 'upper left', bbox_to_anchor=(1,1), frameon=False, ncol=1, title='', labels= labels.values() )
ax.set(xlabel = r'$\mathrm{MSD(t_0)}$')

plt.gcf().set_dpi(400)

#%%plot msds vs time (based on msds)
fig, ax = plt.subplots()
fig.set_size_inches(5,4)

for i, key in enumerate(keys):
    ax.plot(msds[key].index, msds[key], alpha = 0.05, label = key, color = colors[key] )
    _, med, _ = get_median_particle(msds[key], t0)
    ax.plot(med.index, med, '--', alpha=1, label=key, color=medcolors[key], linewidth=2, zorder=5)

ax.set(#ylabel=r'$\langle \Delta r^2 \rangle$ [$\mathrm{\mu m^2}$]',
       ylabel= r'MSD($\tau$) [$\mathrm{\mu m^2}$]',
       xlabel=r'lag time $\tau$ [s]',
       yscale='log',
       xscale='log',
       #xlim = (0.065, None),
       #ylim = (0.5e-5, 217)
       )


ax.axhline(y=Rb, color='k',linewidth=0.5, alpha = 0.5, zorder = 8)
ax.axhline(y=R0, color='k',linewidth=0.5, alpha = 0.5, zorder = 8)
ax.axvline(x=t0, color='k',linewidth=0.5, alpha = 0.5, zorder = 8)

ax.text(y=Rb, x = ax.get_xlim()[1]*1.06, s='$R_b$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(y=R0, x = ax.get_xlim()[1]*1.06, s='$R_0$', rotation=0, verticalalignment='center',  alpha=0.85)
ax.text(x=t0, y = ax.get_ylim()[1]*1.4, s='$t_0$', rotation=0, horizontalalignment='center',  alpha=0.85)

#ax.patch.set_alpha(0)

from matplotlib.lines import Line2D

handles = [Line2D([0], [0], color=colors[key], label=labels[key]) for key in reversed(keys)]
ax.legend(handles = handles, frameon=False)

#fig.suptitle('time average MSD', size=15)
#plt.tight_layout()


fig.set_dpi(800)


#%% bar plot
my_cmap = plt.get_cmap("tab20c")

if remove_stuck:
    if 'free' in ers.index:    
        ers = ers.drop(index='free')

# Adjust the width and spacing of the bars
bar_width = .5  # Adjust the width of the bars
x_positions = np.array([0, .5])  # Adjust the x-positions of the bars

# Create the bar plot with adjusted width and spacing
frequencies.T.plot.bar(width=bar_width, stacked=True, legend=False, yerr = ers.T,
                       capsize=4, align='center')#position = x_positions, )#xticks=x_positions)

fig = plt.gcf()
ax = plt.gca()

fig.set_size_inches(2,4)

#groups = 
'''
hatch_patterns = ['xx', 'xx', '..', '..', '', '']
colors = [r, b] * 3
'''

hatch_patterns = ['']*len(keys)*len(groups)


newcolors = list([colors[key] for key in keys])* len(groups)
newalphas = np.repeat([1,0.5,0.2], len(keys))
newlabels = {key: value.replace(' ', '\n') for key, value in labels.items()}

if remove_stuck:
    newalphas  = np.repeat([1,0.5], len(keys))

# Apply the colors and hatch patterns to the bars
for i, bar in enumerate(ax.patches):
    bar.set(hatch=hatch_patterns[i],
             facecolor=newcolors[i],
             alpha = newalphas[i],
             edgecolor='k',
             )
    
ax.set(#xlabel='Condition',
       ylabel='Percentage')



# Set the x-axis tick positions and labels with line breaks, and rotate them to be horizontal
#ax.set_xticks(x_positions)
ax.set_xticklabels([newlabels[key] for key in keys], rotation=0, fontsize='small')

# Create a legend with a specific location (e.g., 'upper right')
from matplotlib.patches import Patch


order= [6,3,0,7,4,1,8,5,2]
order = [i + len(keys)* j for i in range(len(keys)) for j in reversed(range(len(groups)))]

legend_elements = [Patch(facecolor=newcolors[o], alpha = newalphas[o], edgecolor='k', hatch = '', label='') for o in order]
[element.set_label(groups[i].capitalize()) for i, element in enumerate(legend_elements[-len(groups):])]

# Set the location of the legend ('upper right' in this example)
ax.legend(handles=legend_elements, loc='center right', fontsize = 'small',frameon=False,
          ncol=len(keys), handletextpad=0.5, handlelength=1.0, columnspacing=-0.5,
          #bbox_to_anchor=(-.3, .9),
          bbox_to_anchor=(1.9, .9)
          )


#fig.suptitle('Relative Fraction of \n Particle Motion Groups')
fig.set_dpi(400)
plt.show()


#%% calc ratio between bound and stuck particles
conf = data.loc[(data['motion']!='free')]

nums = data.groupby(['key', 'motion']).count().drop('name',axis=1)
stucks = nums.loc[(slice(None), 'stuck'), 'msd_t0'].droplevel('motion')
bounds = nums.loc[(slice(None), 'bound'), 'msd_t0'].droplevel('motion')
rats = (stucks/bounds).reset_index()

plt.figure()
rats.plot(kind='bar')
fig = plt.gcf()
ax = plt.gca()
fig.set_size_inches(3,4)

newcolors = list([colors[key] for key in keys])
newalphas = np.repeat(0.9, len(keys))
newlabels = {key: value.replace(' ', '\n') for key, value in labels.items()}

for i, bar in enumerate(ax.patches):
    bar.set(#hatch=hatch_patterns[i],
             facecolor=newcolors[i],
             alpha = newalphas[i],
             edgecolor='k',
             )
ax.set_ylabel('$ n_{stuck}/n_{bound}$', labelpad=15)
ax.set_xticklabels([newlabels[key] for key in keys], rotation=0, fontsize='small')
ax.get_legend().remove()
fig.set_dpi(400)
#fig.tight_layout()

