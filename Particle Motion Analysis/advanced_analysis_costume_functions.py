# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 11:19:08 2023

@author: Yael
"""
import numpy as np
import pandas as pd
from cycler import cycler
import matplotlib.pyplot as plt
import trackpy as tp
import itertools
import pickle
import os

#%%
def save_variables_to_file(filename, *variables):
    """
    Save Python variables to a file using pickle.

    :param filename: The name of the file to save the variables to.
    :param variables: One or more Python variables to save.
    """
    try:
        with open(filename, 'wb') as file:
            for var in variables:
                pickle.dump(var, file)
        print(f'Successfully saved variables to {filename}')
    except Exception as e:
        print(f'Error saving variables: {e}')
              
def load_variables_from_file(filename):
    """
    Load Python variables from a file using pickle.

    :param filename: The name of the file to load variables from.
    :return: A tuple of loaded variables.
    """
    loaded_variables = ()
    try:
        with open(filename, 'rb') as file:
            while True:
                try:
                    variable = pickle.load(file)
                    loaded_variables += (variable,)
                except EOFError:
                    break
        return loaded_variables
    except Exception as e:
        print(f'Error loading variables: {e}')


def flatten(l):
    return [item for sublist in l for item in sublist]


def get_lowest_dirs(starting_directory):
    lowest_dirs = list()

    for root,dirs,files in os.walk(starting_directory):
        if not dirs:
            lowest_dirs.append(root)
    return lowest_dirs

def parse_info_file(file_path):
    data = {}
    with open(file_path, 'r') as file:
        for line in file:
            if line.strip():  # Check if the line is not empty
                key, value = line.strip().split(': ')
                data[key.strip()] = value.strip(',')
    return data
#%%

#%%
def div_pars(msd_section, R0, Rb):
    t0=msd_section.index[0]  #get the time used the create the section
    
    free = [col for col in msd_section if msd_section[col][t0]>Rb]
    bound = [col for col in msd_section if (msd_section[col][t0]<Rb and msd_section[col][t0]>R0)]
    stuck = [col for col in msd_section if msd_section[col][t0]<R0]

    return dict(free=free, bound=bound, stuck=stuck)
#%%

def divide_traj_by_section(traj, msd_section, R0, Rb, f_div_par=div_pars, return_subcat=False):    
    traj= traj.copy()  #prevent mutation of the original trajectory
    
    par_groups = f_div_par(msd_section, R0, Rb)
    
    traj['motion'] = (
        np.select(
            condlist = [traj['particle'].isin(group) for group in par_groups.values()],
            choicelist = [group for group in par_groups],
            default=None))
    
    traj_free = traj[traj['motion']=='free']
    traj_bound = traj[traj['motion']=='bound']
    traj_stuck = traj[traj['motion']=='stuck']
    traj_confined = pd.concat([traj_bound, traj_stuck]).sort_values(['frame', 'particle'])
    
    
    
    if return_subcat:
        return traj, traj_free, traj_bound, traj_stuck, traj_confined
    else:
        return traj

#%%
def get_median_particle(msd, t0):
    section = msd[msd.index==t0].squeeze()
    section.sort_values(inplace=True)
    l = round(len(section.index)/2)
    idx = section.index[l]
    return idx, msd[idx], msd[idx][t0]


#%%

def divide_traj_by_MSD(traj, msd, t0, R0, Rb, f_div_par= div_pars):  
    traj= traj.copy()  #prevent mutation of the original trajectory
    
    section = msd[msd.index==t0]
    par_groups = f_div_par(section, R0, Rb)
    
    traj['motion'] = (
        np.select(
            condlist = [traj['particle'].isin(group) for group in par_groups.values()],
            choicelist = [group for group in par_groups],
            default=None))
    
    '''
    traj_free = traj[traj['motion']=='free']
    traj_bound = traj[traj['motion']=='bound']
    traj_stuck = traj[traj['motion']=='stuck']
    traj_confined = pd.concat([traj_bound, traj_stuck]).sort_values(['frame', 'particle'])
    '''

    return traj

#%%
def get_groups(traj_g):
    groups = list(traj_g.groupby('motion').groups.keys())
    if 'free' in groups: groups.remove('free')
    return ['free']+groups


#%%

def traj_group_freqs(traj_g):
    df=pd.DataFrame(columns=['num','freq'])
    groups = get_groups(traj_g)
    
    N = traj_g['particle'].nunique() #total number of particles
    if N==0:
        return pd.DataFrame(0, columns = ['num','freq'], index = groups) 
    
    for group in groups:
        n = traj_g['particle'][traj_g['motion']==group].nunique()
        df.loc[group]=[n, n/N*100]
    return df
#%%


    
#%%
plt_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
default_colors = ['r', 'k'] + plt_colors

#%%
def plot_traj_subgroups(traj_g, costume_cycler= cycler(color=default_colors)):

    groups = get_groups(traj_g) #get names of motion groups
    
    fig, ax1 = plt.subplots() #initialize figure
    ax1.set_prop_cycle(costume_cycler) #set the costume cycler to color the particle
    
    for group in groups: #plot and color the particles that are not free
        if group != 'free': 
            subtraj = traj_g[traj_g['motion']==group] 
            tp.plot_traj(subtraj, colorby = 'frame', label=True, ax = ax1)
            locs = subtraj.groupby('particle')[['x', 'y']].mean()
            ax1.plot(locs['x'], locs['y'], marker='o', lw=0, ms= 12, fillstyle = 'none', label = group)
    
    #now plot free particles:
    try:
        ax1.set_prop_cycle(cycler(color=plt_colors))
        subtraj = traj_g[traj_g['motion']=='free']
        tp.plot_traj(subtraj, colorby = 'particle', label=True, ax = ax1)
    except: None
    
    
    freqs = traj_group_freqs(traj_g)['freq']
    freqs = round(freqs)
    #ax1.set_aspect('equal', 'box')
    ax1.invert_yaxis()
    ax1.axis('square')
    ax1.invert_yaxis()
    
    ax1.legend(bbox_to_anchor = (1.04, 1), loc='upper left')
    ax1.text(1.04, 0.04,
             freqs.to_string(),
             bbox={'facecolor': 'yellow', 'alpha': 0.5, 'pad': 2},
             transform =ax1.transAxes)
    
    return fig, ax1

#%%
'''
list of other functions I still need to write:
    1. for a single file:
        1. MSD with particle numbers V
        2. MSD divided to subgroups (this will take time) V
        3. histogram in line form
        4. the combined MSD-histogram monstrosity (also quite some time)
        
    2. a function that loops over smoothing sizes to determine whats best (easy)
    3. in the other file- a functoin for the 4 lines that repeate at every single cell ,regarding the calling of the trajectory and it's filteration
    4. a function that takes a devided traj and removes drift from it's free particles.
    5. a function that combines trajectories from multiple files into one.
'''


#%%
def msd_with_parnum(time_msd, *args, **kwargs):
    fig, ax = plt.subplots()
    ax.plot(time_msd.index, time_msd, *args, **kwargs)
    
    for parnum in time_msd.columns:
        end = time_msd[parnum].dropna().index[-1]
        ax.text(end, time_msd[parnum][end], str(parnum))
    
    ax.set(ylabel=r'$\langle \Delta r^2 \rangle$ [$\mu$m$^2$]',
           xlabel='lag time $t$ (sec)')
    fig.suptitle('time average MSD')
    
    return fig, ax

#%%
#def line_hist()

#%%
def emsd_per_group(traj_g, mpp, fps, max_lagtime):
    groups = get_groups(traj_g)
    emsd_g = pd.DataFrame()
    
    for group in groups:
        subtraj = traj_g[traj_g['motion']==group] 
        try:
            emsd = tp.emsd(subtraj, mpp, fps, max_lagtime)
            emsd.index = np.round(emsd.index, 14) #to round errors at the 16th digit
            #emsd_g = pd.concat([emsd_g, emsd], axis=1)
        except: emsd = None
        emsd_g[group] = emsd
    #emsd_g.sort_index(inplace=True) #use this in case you don't want to round.
    
    #emsd_g.columns=groups
    return emsd_g
#%%
def get_par_groups(traj_g):
    groups = get_groups(traj_g)
    d = {group: traj_g['particle'][traj_g['motion']==group].unique() 
         for group in groups}
    return d
#%%
def imsd_per_group(traj_g, msd):
    groups = get_groups(traj_g)
    pars = get_par_groups(traj_g)
    imsd_g = {group: msd[pars[group]] for group in groups}
    return imsd_g
#%%
def plot_groups_msd(traj_g, imsd_g, emsd_g, ax=None, name="", m='o', **kwargs):
    groups = get_groups(traj_g)
    
    if ax==None:
        fig, ax1 = plt.subplots()
    else: 
        fig = plt.gcf()
        ax1 = ax
    
    #plot time and ensemble MSD for the different groups:
    freqs = traj_group_freqs(traj_g)['freq']/100
    
    p = []
    for j, group in enumerate(groups):
        p.append(ax1.plot(emsd_g[group].index, emsd_g[group], m, lw=3, ms=3, label=name+group, alpha = freqs[group]))
        #xycor = (emsd_g[group].index, emsd_g[group].iloc[-1])
        #ax1.annotate(str(round(freqs[group]*100, 2)) + '%' ,
         #            xy = (1.01, emsd_g[group].max(axis=0)), xycoords = ('axes fraction', 'data') )
        
    #flatten the resulting double list
    p = [item for sublist in p for item in sublist]
    
    #p = ax1.plot(emsd_g.index, emsd_g, 'o', ms=2, label=name+emsd_g.columns)
    
    #ax1.legend(emsd_g.columns) #put legend now because it will be impossible later
    
    #now plot single particles' MSDs:
    for i, group in enumerate(groups):
        ax1.plot(imsd_g[group].index, imsd_g[group], color=p[i].get_color(), linestyle='-', alpha= 0.0)
    
    ax1.legend()
    
    ax1.set(ylabel=r'$\langle \Delta r^2 \rangle$ [$\mu$m$^2$]',
           xlabel='lag time $t$ (sec)')
    
    ax1.autoscale()
    return fig, ax1

#%%
def hist_per_group(traj_g, imsd_g, t0, **kwargs):
    groups = get_groups(traj_g)
    
    kwargs['density'] = None
    if type(kwargs['bins'])!=int:
        raise ('bins are not equal')
        
        
    #rejoin msds for histogram of all particles:
    imsd = pd.concat([imsd_g[group] for group in groups], axis=1 )
    #histogram of all groups:
    section = imsd[imsd.index==t0]
    counts, bins = np.histogram(section.values.flatten(), **kwargs)
    #normalize:
    N = sum(counts)
    binwidth = bins[1]-bins[0]
    dens = counts/(N*binwidth)
    hist_all = (dens, bins)
    
    if kwargs['range'] == None and type(kwargs['bins'])==int:
        small = section.max(axis=1).values[0]
        big = section.max(axis=1).values[0]
        kwargs['range'] = (small*0.99, big*1.01)
    
    #now for individual histograms:
    hist_g = {}
    for group in groups:
        subsection = imsd_g[group][imsd_g[group].index==t0]
        counts, bins= np.histogram(subsection.values.flatten(), **kwargs)
        #normalize
        dens = counts/(N*binwidth)
        hist_g[group]= (dens, bins)
    
    
    return hist_all, hist_g



#%%
def plot_line_hist(traj_g, imsd_g, t0, ax=None, name = "", **kwargs):
    groups = get_groups(traj_g)
    
    if ax==None:
        fig, ax2 = plt.subplots()
    else: 
        fig = plt.gcf()
        ax2 = ax
    
    hist_all, hist_g = hist_per_group(traj_g, imsd_g, t0, **kwargs)    
    

    ax2.plot(hist_all[1][:-1], hist_all[0], label=(name+'joined'))
    
    for group in groups:
        subhist = hist_g[group]
        ax2.plot(subhist[1][:-1], subhist[0], alpha=0.1)
        ax2.fill_between(subhist[1][:-1], subhist[0], alpha =0.4, label=(name+group))
    
    ax2.set(xlabel=('MSD' + f' at t0={t0}  [$\mu m ^2$]'),
           ylabel='density')
    ax2.legend()
    return fig, ax2
#%%

def plot_line_hist_h(traj_g, imsd_g, t0, ax=None, name = "", **kwargs):
    groups = get_groups(traj_g)
    
    if ax==None:
        fig, ax2 = plt.subplots()
    else: 
        fig = plt.gcf()
        ax2 = ax
    
    hist_all, hist_g = hist_per_group(traj_g, imsd_g, t0, **kwargs)    
    

    ax2.plot(hist_all[0], hist_all[1][:-1], label=(name+'joined'))
    
    for group in groups:
        subhist = hist_g[group]
        ax2.plot(subhist[0], subhist[1][:-1], alpha=0.1)
        ax2.fill_between(subhist[0], subhist[1][:-1], alpha =0.4, label=(name+group))
    
    ax2.set(ylabel=('MSD' + f' at t0={t0}  [$\mu m ^2$]'),
           xlabel='density')
    ax2.legend()
    return fig, ax2
    

#%%
'''
def msd_n_hist(ax1, ax2):
    
  '''      

#%%
def find_max_points(ax):
    lines = ax.get_lines()
    points = []
    for line in lines:
        yd = line.get_ydata()
        xd = line.get_xdata()
        max_point = (xd[yd.argmax()],yd.max())
        points.append(max_point)
    return points

def find_mix_points(ax):
    lines = ax.get_lines()
    points = []
    for line in lines:
        yd = line.get_ydata()
        xd = line.get_xdata()
        min_point = (xd[yd.argmin()],yd.min())
        points.append(min_point)
    return points

#%%
def bootstrap_samples(ser, stat_func=np.mean, n_samples = 1000):
    sample_size = len(ser)
    # create a list for sample means
    res = []
    
    # loop n_samples times
    for i in range(n_samples):
        
        # create a bootstrap sample of sample_size with replacement
        sample = ser.sample(n = sample_size, replace = True)
        
        # calculate the bootstrap sample statistic
        stat = stat_func(sample)
        
        # add this sample mean to the sample means list
        res.append(stat)
    
    return pd.Series(res)


def stat_CI(ser, alpha, stat_func=np.mean, n_samples=1000, mod='err'):
    boot = bootstrap_samples(ser, stat_func, n_samples)
    if alpha>0.5: 
        alpha = 1-alpha
    alpha = alpha/2
    
    quantiles = boot.quantile(alpha), boot.quantile(1-alpha)
    stat = stat_func(ser)
    
    if mod == 'range':
        #print('stat in range', quantiles)
        return stat, quantiles
    if mod == 'err':
        err = (np.diff(quantiles)/2)[0]
        #print (f'stat = {round(stat,3)} +- {round(err,3)}')
        return stat, err